<?php

class App_Model_DAO_Clientes extends App_Model_DAO_Abstract
{
	public static $instance = null;

	protected $_name = 'clientes';
	protected $_primary = 'cli_idCliente';
	protected $_rowClass = 'App_Model_Entity_Cliente';
	
	protected $_dependentTables = array(
		'App_Model_DAO_Pedidos'
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Clientes
	 */
	public static function getInstance()
	{
		if (self::$instance == null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Gera uma senha rand�mica
	 *
	 * @param integer $digitos
	 * @return string
	 */
	public function createPassword($digits = 5)
	{
		$string = '0123456789abcdefghijlmnopqrstuvxyzw';
		$password = '';
		while (strlen($password) < $digits) {
			$password .= substr($string, rand(0, strlen($string)), 1);
		}
		return $password;
	}
	
}