<?php

class Produtos_GaleriaController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {			
			case 'categorias':
				$retorno = array();
				$daoCategorias = App_Model_DAO_Categorias::getInstance();
				$select = $daoCategorias->select()->order('cat_ordem ASC');
				if (false != ($node = $this->getRequest()->getParam('node', false))) {
					$select->where('cat_idCategoriaPai = ?', $node);
				} else {
					$select->where('ISNULL(cat_idCategoriaPai)');
				}

				$rsCategorias = $daoCategorias->fetchAll($select);
				foreach ($rsCategorias as $categoria) {
					$retorno[] = array(
						'id' => $categoria->getCodigo(),
						'text' => $categoria->getNome(),
						'cls' => ($categoria->getStatus() == false ? 'tree-status-inativo' : ''),
						'iconCls' => ($categoria->getChilds()->count() ? 'icon-folder-pai' : 'icon-folder'),
						'leaf' => ($categoria->getChilds()->count() ? false : true),
						'status' => $categoria->getStatus(),
						'produtos' => array()
					);
				}
				unset($daoCategorias, $rsCategorias);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;

			case 'produtos':
				$retorno = array();
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$daoProdutosCategorias = App_Model_DAO_Produtos_Categorias::getInstance();

				$select = $daoProdutos->getAdapter()->select()
					->from($daoProdutos->info('name'))
					->joinInner($daoProdutosCategorias->info('name'), 'prod_cat_idProduto = prod_idProduto', null);

				if (false != ($idGaleria = $this->getRequest()->getParam('galeria'))) {
					$select->where('prod_cat_idCategoria = ?', $idGaleria);
				}
				if (false != ($idMarca = $this->getRequest()->getParam('marca'))) {
					$select->where('prod_idMarca = ?', $idMarca);
				}
				if (false != ($filtro = $this->getRequest()->getParam('busca'))) {
					$select->where('prod_nome LIKE ?', "%{$filtro}%")->group('prod_idProduto');
				}
				
				$rsProdutos = $daoProdutos->createRowset(
					$daoProdutos->getAdapter()->fetchAll($select)
				);

				switch ($this->getRequest()->getParam('type')) {
					case '1': //exibe todos os produtos e SKUs
						$listaProdutos = array();
						foreach ($rsProdutos as $produto) {
							$listaProdutos[] = array(
								'prod_idProduto' => $produto->getCodigo(),
								'prod_tipo' => 'P',
								'prod_nome' => $produto->getNome(),
								'prod_preco' => $produto->getVitrine()->getPrecoOriginal()
							);
							foreach ($produto->getSKU() as $sku) {
								$listaProdutos[] = array(
									'prod_idProduto' => $sku->getSKU(),
									'prod_tipo' => 'S',
									'prod_nome' => $sku->getNome(),
									'prod_preco' => $sku->getPrecoOriginal()
								);
							}
						}
						$retorno['produtos'] = $listaProdutos;
						break;

					case '2': //exibe somente os produtos
						foreach ($rsProdutos as $produto) {
							$retorno['produtos'][] = array(
								'prod_idProduto' => $produto->getCodigo(),
								'prod_tipo' => 'P',
								'prod_nome' => $produto->getNome(),
								'prod_preco' => $produto->getVitrine()->getPreco()
							);
						}
						break;

					case '3': //exibe somente os SKUs
						foreach ($rsProdutos as $produto) {
							foreach ($produto->getSKU() as $sku) {
								$retorno['produtos'][] = array(
									'prod_idProduto' => $sku->getSKU(),
									'prod_tipo' => 'S',
									'prod_nome' => $sku->getNome(),
									'prod_preco' => $sku->getPreco()
								);
							}
						}
						break;

					case '4': //exibe produtos e SKUs escondendo o SKU quando o produto tiver somente uma varia��o
						$listaProdutos = array();
						foreach ($rsProdutos as $produto) {
							$listaProdutos[] = array(
								'prod_idProduto' => $produto->getCodigo(),
								'prod_tipo' => 'P',
								'prod_nome' => $produto->getNome(),
								'prod_preco' => $produto->getVitrine()->getPreco()
							);
							if ($produto->getSKU()->count() > 1) {
								foreach ($produto->getSKU() as $sku) {
									$listaProdutos[] = array(
										'prod_idProduto' => $sku->getSKU(),
										'prod_tipo' => 'S',
										'prod_nome' => $sku->getNome(),
										'prod_preco' => $sku->getPreco()
									);
								}
							}
						}
						$retorno['produtos'] = $listaProdutos;
						break;
					}
					unset($daoProdutos, $daoProdutosCategorias, $rsProdutos);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				break;

			case 'resizeprodutos':
			case 'resizesku':
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$produto = $daoProdutos->fetchRow($daoProdutos->select()->where('prod_idProduto = ?', $this->getRequest()->getParam('id')));
				if ($produto->getVitrine()->getImagens()->count()) {
					$this->getResponse()->clearHeader('Content-Type');
					$this->_forward('resize-galeria', 'utils', null, array(
						'imagem' => $produto->getVitrine()->getImagens()->current()->getCodigo(),
						'width' => $this->getRequest()->getParam('width'),
						'height' => $this->getRequest()->getParam('height')
					));
				}
				unset($daoProdutos, $produto);
				break;

			case 'resizesku':
				$daoProdutosSKU = App_Model_DAO_Produtos_SKU::getInstance();
				$sku = $daoProdutosSKU->fetchRow($daoProdutosSKU->select()->where('prod_sku_SKU = ?', $this->getRequest()->getParam('id')));
				if ($sku->getImagens()->count()) {
					$this->getResponse()->clearHeader('Content-Type');
					$this->_forward('resize-galeria', 'utils', null, array(
						'imagem' => $sku->getImagens()->current()->getCodigo(),
						'width' => $this->getRequest()->getParam('width'),
						'height' => $this->getRequest()->getParam('height')
					));
				}
				unset($daoProdutosSKU, $sku);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}
}