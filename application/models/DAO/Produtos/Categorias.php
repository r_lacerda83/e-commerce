<?php

class App_Model_DAO_Produtos_Categorias extends App_Model_DAO_Abstract
{
	protected static $instance;

	protected $_name = 'produtos_categorias';
	protected $_primary = array('prod_cat_idProduto', 'prod_cat_idCategoria');
	protected $_sequence = false;

	protected $_referenceMap = array(
		'Produto' => array(
			self::COLUMNS => 'prod_cat_idProduto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos',
			self::REF_COLUMNS => 'prod_idProduto'
		),
		'Categoria' => array(
			self::COLUMNS => 'prod_cat_idCategoria',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Categorias',
			self::REF_COLUMNS => 'cat_idCategoria'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Produtos_Categorias
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}