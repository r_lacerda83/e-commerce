<?php

class App_Model_DAO_Produtos_SKU_Imagens extends App_Model_DAO_Abstract
{
	protected static $instance;

	protected $_name = 'produtos_sku_imagens';
	protected $_primary = array('prod_sku_img_SKU', 'prod_sku_img_idImagem');
	protected $_sequence = false;

	protected $_referenceMap = array(
		'SKU' => array(
			self::COLUMNS => 'prod_sku_img_SKU',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos_SKU',
			self::REF_COLUMNS => 'prod_sku_SKU'
		),
		'Imagem' => array(
			self::COLUMNS => 'prod_sku_img_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Produtos_SKU_Imagens
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}