<?php

class App_Model_DAO_Produtos_SKU_Atributos extends App_Model_DAO_Abstract
{
	protected static $instance;

	protected $_name = 'produtos_sku_atributos';
	protected $_primary = array('prod_sku_atr_SKU', 'prod_sku_atr_idAtributo');
	protected $_sequence = false;

	protected $_referenceMap = array(
		'SKU' => array(
			self::COLUMNS => 'prod_sku_atr_SKU',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos_SKU',
			self::REF_COLUMNS => 'prod_sku_SKU'
		),
		'Atributo' => array(
			self::COLUMNS => 'prod_sku_atr_idAtributo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Variacoes_Atributos',
			self::REF_COLUMNS => 'var_atr_idAtributo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Produtos_SKU_Atributos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}