<?php

class App_Model_DAO_Produtos_Variacoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'produtos_variacoes';
	protected $_primary = array('prod_var_idProduto', 'prod_var_idVariacao');
	protected $_sequence = false;

	protected $_referenceMap = array(
		'Produto' => array(
			self::COLUMNS => 'prod_var_idProduto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos',
			self::REF_COLUMNS => 'prod_idProduto'
		),
		'Variacao' => array(
			self::COLUMNS => 'prod_var_idVariacao',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Variacoes',
			self::REF_COLUMNS => 'var_idVariacao'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Produtos_Variacoes
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}