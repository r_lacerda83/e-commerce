<?php

class App_Model_DAO_Produtos_SKU extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'produtos_sku';
	protected $_primary = 'prod_sku_SKU';
	protected $_rowClass = 'App_Model_Entity_Produto_SKU';

	protected $_dependentTables = array(
		'App_Model_DAO_Produtos_SKU_Atributos',
		'App_Model_DAO_Produtos_SKU_Imagens',
		'App_Model_DAO_Pedidos_Items'
	);

	protected $_referenceMap = array(
		'Produto' => array(
			self::COLUMNS => 'prod_sku_idProduto',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos',
			self::REF_COLUMNS => 'prod_idProduto'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Produtos_SKU
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function generateSKU($digits = 12)
	{
		$string = '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZ';
		do {
			$sku = null;
			while (strlen($sku) < $digits) {
				$sku .= substr($string, rand(0, strlen($string)), 1);
			}
		} while ($this->getAdapter()->fetchOne($this->select()->where('prod_sku_SKU = ?', $sku)) != null);

		return $sku;
	}

	public function sendAvisoEstoque(App_Model_Entity_Produto_SKU $sku)
	{		
		$mail_template = new Zend_View();
		$mail_template->setBasePath(APPLICATION_ROOT . '/modules/admin/views');
		$mail_template->sku = $sku;

		$mailTransport = new Zend_Mail_Transport_Smtp(Zend_Registry::get('config')->mail->host, Zend_Registry::get('config')->mail->smtp->toArray());

		$mail = new Zend_Mail('ISO-8859-1');
		$mail->setReplyTo(Zend_Registry::get('config')->mail->pedido->toArray());
		$mail->setFrom(Zend_Registry::get('config')->mail->webmaster, Zend_Registry::get('config')->project);
		$mail->addTo(Zend_Registry::get('config')->mail->pedido->toArray());
		$mail->setSubject(Zend_Registry::get('config')->project . " - Estoque min�mo atingido - {$sku->getSKU()}");
		$mail->setBodyHtml($mail_template->render('pedidos/templateEstoque.phtml'));
		$mail->send($mailTransport);
		unset($mail, $mailTransport);
	}
}