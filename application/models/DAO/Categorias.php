<?php

class App_Model_DAO_Categorias extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'categorias';
	protected $_primary = 'cat_idCategoria';
	protected $_rowClass = 'App_Model_Entity_Categoria';

	protected $_dependentTables = array(
		'App_Model_DAO_Produtos_Categorias'
	);

	protected $_referenceMap = array(
		'Filho' => array(
			self::COLUMNS => 'cat_idCategoriaPai',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Categorias',
			self::REF_COLUMNS => 'cat_idCategoria'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Categorias
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function delete($where)
	{
		try {
			$this->getAdapter()->beginTransaction();
			$transaction = true;
		} catch (Exception $e) {
			$transaction = false;
		}

		try {
			$objCategoria = $this->fetchRow($where);
			foreach ($objCategoria->getChilds() as $child) {
				$child->delete();
			}
			parent::delete($where);
			if ($transaction) {
				$this->getAdapter()->commit();
			}
		} catch (Exception $e) {
			if ($transaction) {
				$this->getAdapter()->rollBack();
			}
			throw $e;
		}
	}
}