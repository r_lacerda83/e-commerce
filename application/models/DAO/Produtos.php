<?php

class App_Model_DAO_Produtos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'produtos';
	protected $_primary = 'prod_idProduto';
	protected $_rowClass = 'App_Model_Entity_Produto';

	protected $_dependentTables = array(
		'App_Model_DAO_Produtos_Variacoes',
		'App_Model_DAO_Produtos_SKU',
		'App_Model_DAO_Produtos_Categorias',
	);

	protected $_referenceMap = array(
		'Marca' => array(
			self::COLUMNS => 'prod_idMarca',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Marcas',
			self::REF_COLUMNS => 'marc_idMarca'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Produtos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * @return Zend_Db_Select
	 */
	public function buscaProduto()
	{
		$daoProdutosSKU = App_Model_DAO_Produtos_SKU::getInstance();
		$daoProdutosVariacoes = App_Model_DAO_Produtos_Variacoes::getInstance();
		$daoOfertas = App_Model_DAO_Descontos_Ofertas::getInstance();
		$daoProdutosSKUAtributos = App_Model_DAO_Produtos_SKU_Atributos::getInstance();
		$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
		$daoVariacoesAtributos = App_Model_DAO_Variacoes_Atributos::getInstance();

		$select = $this->getAdapter()->select()
			->from(array(
				'produtos' => $this->getAdapter()->select()->from(array(
					'produtos' => $this->getAdapter()->select()->union(array(
						$this->getAdapter()->select()
							->from($this->info('name'))
							->joinInner($daoProdutosSKU->info('name'), 'prod_sku_idProduto = prod_idProduto and prod_sku_status = 1', array('SKU' => 'prod_sku_SKU', 'prod_preco' => new Zend_Db_Expr('FORMAT(IF(ISNULL(oferta.ofe_valor), prod_sku_preco, IF(oferta.ofe_tipoDesconto = "V", oferta.ofe_valor, (prod_sku_preco - ((prod_sku_preco / 100) * oferta.ofe_valor)))), 2) * 1'), 'prod_oferta' => new Zend_Db_Expr('IF (ISNULL(oferta.ofe_idProduto), FALSE, TRUE)')))
							->joinLeft(array('oferta' => $daoOfertas->select()->from($daoOfertas, array('ofe_idProduto', 'ofe_tipoDesconto', 'ofe_valor'))->where('ofe_status = ?', 1)->where('NOT ISNULL(ofe_idProduto) AND ISNULL(ofe_SKU)')->where('NOW() BETWEEN ofe_dataInicio AND ofe_dataExpiracao')), 'oferta.ofe_idProduto = prod_idProduto', null)
							->joinInner($daoProdutosSKUAtributos->info('name'), 'prod_sku_atr_SKU = prod_sku_SKU', null)
							->joinInner($daoVariacoesAtributos->info('name'), 'var_atr_idAtributo = prod_sku_atr_idAtributo', null)
							->joinInner($daoVariacoes->info('name'), $this->getAdapter()->quoteInto('var_idVariacao = var_atr_idVariacao AND var_idVariacao = ?', $daoProdutosVariacoes->select()->from($daoProdutosVariacoes, array('prod_var_idVariacao'))->where('prod_var_idProduto = prod_idProduto')->order('prod_var_ordem ASC')->limit(1)), null)
							->where('prod_status = 1')
							->group('prod_idProduto')->group('prod_sku_atr_idAtributo'),

						$this->getAdapter()->select()
							->from($this->info('name'))
							->joinInner($daoProdutosSKU->info('name'), 'prod_sku_idProduto = prod_idProduto AND prod_sku_status = 1', array('SKU' => 'prod_sku_SKU', 'prod_preco' => new Zend_Db_Expr('FORMAT(IF(ISNULL(oferta.ofe_valor), prod_sku_preco, IF(oferta.ofe_tipoDesconto = "V", oferta.ofe_valor, (prod_sku_preco - ((prod_sku_preco / 100) * oferta.ofe_valor)))), 2) * 1'), 'prod_oferta' => new Zend_Db_Expr('IF (ISNULL(oferta.ofe_SKU), FALSE, TRUE)')))
							->joinLeft(array('oferta' => $daoOfertas->select()->from($daoOfertas, array('ofe_SKU', 'ofe_tipoDesconto', 'ofe_valor'))->where('ofe_status = ?', 1)->where('NOT ISNULL(ofe_SKU) AND ISNULL(ofe_idProduto)')->where('NOW() BETWEEN ofe_dataInicio AND ofe_dataExpiracao')), 'oferta.ofe_SKU = prod_sku_SKU', null)
							->joinInner($daoProdutosSKUAtributos->info('name'), 'prod_sku_atr_SKU = prod_sku_SKU', null)
							->joinInner($daoVariacoesAtributos->info('name'), 'var_atr_idAtributo = prod_sku_atr_idAtributo', null)
							->joinInner($daoVariacoes->info('name'), $this->getAdapter()->quoteInto('var_idVariacao = var_atr_idVariacao AND var_idVariacao = ?', $daoProdutosVariacoes->select()->from($daoProdutosVariacoes, array('prod_var_idVariacao'))->where('prod_var_idProduto = prod_idProduto')->order('prod_var_ordem ASC')->limit(1)), null)
							->where('prod_status = 1')
							->group('prod_idProduto')->group('prod_sku_atr_idAtributo'),
					), Zend_Db_Select::SQL_UNION_ALL)
					->group('prod_idProduto')
					->group('prod_sku_atr_idAtributo')
				))
				->order('prod_nome ASC')->order('prod_preco ASC')
			))
			->group('SKU')
			->order('prod_nome ASC');

		unset($daoProdutosSKU, $daoProdutosCategorias, $daoCategorias, $daoMarcas, $daoOfertas);

		return $select;
	}
}