<?php

class App_Model_DAO_Pedidos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos';
	protected $_primary = 'ped_idPedido';
	protected $_rowClass = 'App_Model_Entity_Pedido';

	protected $_dependentTables = array(
		'App_Model_DAO_Pedidos_Items',
		'App_Model_DAO_Pedidos_Entrega',
	);

	protected $_referenceMap = array(
		'Cliente' => array(
			self::COLUMNS => 'ped_idCliente',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Clientes',
			self::REF_COLUMNS => 'cli_idCliente'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Pedidos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

}