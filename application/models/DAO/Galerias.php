<?php

class App_Model_DAO_Galerias extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'galerias';
	protected $_primary = 'gal_idGaleria';
	protected $_rowClass = 'App_Model_Entity_Galeria';
	
	protected $objGalerias = null;

	protected $_dependentTables = array(
		'App_Model_DAO_Galerias_Arquivos'
	);

	/**
	 * Implementação do método Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Galerias
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function getGalerias() 
	{
		if (null == $this->objGalerias) {
			$this->objGalerias = new ArrayObject();

			//varre o diretório Entrega/Forma procurando as classes disponíveis
			$path = APPLICATION_ROOT . '/models/Entity/Galeria/Galerias';
			$classes = new DirectoryIterator($path);
			foreach ($classes as $file) {
				if ($file->isDot() || $file->isDir()) {
					continue;
				}
				
				$classname = "App_Model_Entity_Galeria_Galerias_{$file->getBasename('.php')}";
				if (class_exists($classname)) {
					$class = new $classname();
					$this->objGalerias->append($class);
				}
			}
		}
		return $this->objGalerias;
	}
	
	public function getGaleria($tipo) {
		$retorno = null;
		
		foreach($this->getGalerias() as $galeria) {
			if($galeria->getCodigo() == $tipo) {
				$retorno = $galeria;
				break;
			}
		}
		return $retorno;
	}
	
}