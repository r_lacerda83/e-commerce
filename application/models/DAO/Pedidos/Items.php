<?php

class App_Model_DAO_Pedidos_Items extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_items';
	protected $_primary = 'ped_item_idItem';
	protected $_rowsetClass = 'App_Model_Entity_Pedido_Items';
	protected $_rowClass = 'App_Model_Entity_Pedido_Item';
	
	protected $_referenceMap = array(
		'Pedido' => array(
			self::COLUMNS => 'ped_item_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		),
		'SKU' => array(
			self::COLUMNS => 'ped_item_sku',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Produtos_SKU',
			self::REF_COLUMNS => 'prod_sku_SKU'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Items
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}