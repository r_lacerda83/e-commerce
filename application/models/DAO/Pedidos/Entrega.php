<?php

class App_Model_DAO_Pedidos_Entrega extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_entrega';
	protected $_primary = 'ped_ent_idPedido';
	protected $_rowClass = 'App_Model_Entity_Pedido_Entrega';
	
	protected $_referenceMap = array(
		'Pedido' => array(
			self::COLUMNS => 'ped_ent_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Entrega
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}