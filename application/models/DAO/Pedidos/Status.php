<?php

class App_Model_DAO_Pedidos_Status extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'pedidos_status';
	protected $_primary = 'ped_status_idStatus';
	protected $_rowClass = 'App_Model_Entity_Pedido_Status';
	
	protected $_referenceMap = array(
		'Pedido' => array(
			self::COLUMNS => 'ped_status_idPedido',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Pedidos',
			self::REF_COLUMNS => 'ped_idPedido'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Pedidos_Status
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}