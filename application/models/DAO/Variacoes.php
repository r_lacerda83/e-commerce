<?php

class App_Model_DAO_Variacoes extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'variacoes';
	protected $_primary = 'var_idVariacao';
	protected $_rowClass = 'App_Model_Entity_Variacao';

	protected $_dependentTables = array(
		'App_Model_DAO_Variacoes_Atributos',
		'App_Model_DAO_Produtos_Variacoes'
	);

	protected $_referenceMap = array(
		'Imagem' => array(
			self::COLUMNS => 'var_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Galerias
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}