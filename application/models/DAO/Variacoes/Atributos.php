<?php

class App_Model_DAO_Variacoes_Atributos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'variacoes_atributos';
	protected $_primary = 'var_atr_idAtributo';
	protected $_rowClass = 'App_Model_Entity_Variacao_Atributo';

	protected $_dependentTables = array(
		'App_Model_DAO_Produtos_SKU_Atributos'
	);

	protected $_referenceMap = array(
		'Variacao' => array(
			self::COLUMNS => 'var_atr_idVariacao',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Variacoes',
			self::REF_COLUMNS => 'var_idVariacao'
		),
		'Imagem' => array(
			self::COLUMNS => 'var_atr_idImagem',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Galerias
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}