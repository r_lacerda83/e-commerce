<?php

class App_Model_DAO_Galerias_Arquivos extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'galerias_arquivos';
	protected $_primary = 'gal_arq_idArquivo';
	protected $_rowClass = 'App_Model_Entity_Galeria_Arquivo';

	protected $_dependentTables = array(
		'App_Model_DAO_Marcas',
		'App_Model_DAO_Variacoes',
		'App_Model_DAO_Variacoes_Atributos',
		'App_Model_DAO_Produtos_SKU_Imagens',
		'App_Model_DAO_Produtos'
	);

	protected $_referenceMap = array(
		'Galeria' => array(
			self::COLUMNS => 'gal_arq_idGaleria',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias',
			self::REF_COLUMNS => 'gal_idGaleria',
			self::ON_DELETE => self::CASCADE
		),
		'Usuario' => array(
			self::COLUMNS => 'gal_arq_idUsuario',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Sistema_Usuarios',
			self::REF_COLUMNS => 'usr_idUsuario'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Model_DAO_Galerias_Arquivos
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
}