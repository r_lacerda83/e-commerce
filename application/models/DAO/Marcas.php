<?php

class App_Model_DAO_Marcas extends App_Model_DAO_Abstract
{
	protected static $instance = null;

	protected $_name = 'marcas';
	protected $_primary = 'marc_idMarca';
	protected $_rowClass = 'App_Model_Entity_Marca';

	protected $_dependentTables = array(
		'App_Model_DAO_Produtos',
	);

	protected $_referenceMap = array(
		'Logo' => array(
			self::COLUMNS => 'marc_idLogo',
			self::REF_TABLE_CLASS => 'App_Model_DAO_Galerias_Arquivos',
			self::REF_COLUMNS => 'gal_arq_idArquivo'
		)
	);

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 * 
	 * @return App_Model_DAO_Marcas
	 */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}