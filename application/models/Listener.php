<?php

class App_Model_Listener
{
	protected $events = array();

	public function __sleep()
	{
		return array('events');
	}

	/**
	 * 
	 * @param string $event
	 * @param string $function
	 * @param object $scope
	 * @return App_Model_Listener
	 */
	public function on($event, $function, $scope = null)
	{
		$this->events[$event][] = array(
			'fn' => $function,
			'scope' => $scope
		);
		return $this;
	}

	/**
	 * 
	 * @param string $event
	 * @param array $params
	 * @return mixed
	 */
	protected function fireEvent($event, array $params = null)
	{
		$return = null;
		if (isset($this->events[$event]))
		{
			foreach ($this->events[$event] as $listener)
			{
				$call = $listener['scope'] ? array($listener['scope'], $listener['fn']) : $listener['fn'];
				$return = (null !== $params) ? call_user_func_array($call, $params) : call_user_func($call);
			}
		}
		return $return;
	}
}