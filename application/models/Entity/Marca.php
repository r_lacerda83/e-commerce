<?php

class App_Model_Entity_Marca extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Galeria_Arquivo
	 */
	protected $objLogo = null;

	/**
	 * @var App_Model_Entity_Galeria_Arquivo
	 */
	protected $objBanner = null;
	
	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objLogo', 'objBanner'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Marcas::getInstance());
	}

	public function save()
	{
		$this->marc_status = (int) $this->marc_status;

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'marc_idMarca' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'marc_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'marc_idLogo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'marc_tipo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'marc_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			parent::save();
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador do fornecedor
	 *
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->marc_idMarca;
	}

	/**
	 * Define o logotipo
	 *
	 * @param App_Model_Entity_Galeria_Arquivo $value
	 * @return App_Model_Entity_Marca
	 */
	public function setLogo(App_Model_Entity_Galeria_Arquivo $value = null)
	{
		$this->objLogo = $value;
		$this->marc_idLogo = ($value != null ? $value->getCodigo() : null);
		return $this;
	}

	/**
	 * Recupera o logotipo
	 *
	 * @return App_Model_Entity_Galeria_Imagem
	 */
	public function getLogo()
	{
		if (null === $this->objLogo && $this->getCodigo()) {
			$this->objLogo = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Logo');
		}
		return $this->objLogo;
	}

	/**
	 * Define o nome da marca
	 *
	 * @param string $value
	 * @return App_Model_Entity_Marca
	 */
	public function setNome($value)
	{
		$this->marc_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o nome da marca
	 *
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->marc_nome;
	}
	
	/**
	 * Define o tipo da marca
	 *
	 * @param string $value
	 * @return App_Model_Entity_Marca
	 */
	public function setTipo($value)
	{
		$this->marc_tipo = (string) $value;
		return $this;
	}

	/**
	 * Recupera o tipo da marca
	 *
	 * @return string
	 */
	public function getTipo()
	{
		return (string) $this->marc_tipo;
	}

	/**
	 * Define o status da marca
	 *
	 * @param boolean $value
	 * @return App_Model_Entity_Marca
	 */
	public function setStatus($value)
	{
		$this->marc_status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera o status da marca
	 *
	 * @return boolean
	 */
	public function getStatus()
	{
		return (bool) $this->marc_status;
	}
	
}