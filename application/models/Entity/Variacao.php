<?php

class App_Model_Entity_Variacao extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagem = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Variacao_Atributo
	 */
	protected $objAtributos = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objImagem', 'objAtributos'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Variacoes::getInstance());
	}

	/**
	 * Recupera o c�digo identificador da varia��o
	 *
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->var_idVariacao;
	}

	public function setNome($value)
	{
		$this->var_nome = (string) $value;
		return $this;
	}

	public function getNome()
	{
		return (string) $this->var_nome;
	}

	public function setRotulo($value)
	{
		$this->var_rotulo = (string) $value;
		return $this;
	}

	public function getRotulo()
	{
		return (string) $this->var_rotulo;
	}

	public function setImagem(App_Model_Entity_Galeria_Arquivo $value = null)
	{
		$this->objImagem = $value;
		$this->var_idImagem = ($value != null ? $value->getCodigo() : null);
		return $this;
	}

	public function getImagem()
	{
		if (null == $this->objImagem && $this->getCodigo()) {
			$this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
		}
		return $this->objImagem;
	}

	public function getAtributos()
	{
		if (null == $this->objAtributos) {
			if ($this->getCodigo()) {
				$this->objAtributos = $this->findDependentRowset(
					App_Model_DAO_Variacoes_Atributos::getInstance(),
					'Variacao',
					App_Model_DAO_Variacoes_Atributos::getInstance()->select()->order('var_atr_ordem ASC')
				);
				foreach ($this->objAtributos as $atributo) {
					$atributo->setVariacao($this);
				}
				$this->objAtributos->rewind();
			} else {
				$this->objAtributos = App_Model_DAO_Variacoes_Atributos::getInstance()->createRowset();
			}
		}
		return $this->objAtributos;
	}
	
	public function isUnique() {
		return (bool) $this->var_unica;
	}
}