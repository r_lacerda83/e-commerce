<?php

class App_Model_Entity_Produto extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Marca
	 */
	protected $objMarca = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Categoria
	 */
	protected $objCategorias = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Produto_SKU
	 */
	protected $objSKU = null;


	/**
	 * @var App_Model_Collection of App_Model_Entity_Variacoes
	 */
	protected $objVariacoes = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objMarca', 'objCategorias', 'objSKU', 'objVariacoes'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Produtos::getInstance());
	}

	public function save()
	{
		$this->prod_destaqueHome = (int) $this->prod_destaqueHome;
		$this->prod_status = (int) $this->prod_status;
		if (!$this->getCodigo()) $this->prod_dataCadastro = date('Y-m-d H:i:s');

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'prod_idMarca' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 100)
			),
			'prod_descricao' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			$this->getTable()->getAdapter()->beginTransaction();
			$trans = true;
		} catch (Exception $e) {
			$trans = false;
		}
		try {
			parent::save();

			//categorias
			$daoCategorias = App_Model_DAO_Produtos_Categorias::getInstance();
			$this->getCategorias(); //for�a o carregamento das categorias
			$daoCategorias->delete($daoCategorias->getAdapter()->quoteInto('prod_cat_idProduto = ?', $this->getCodigo()));
			foreach ($this->getCategorias() as $categoria) {
				$daoCategorias->insert(array(
					'prod_cat_idProduto' => $this->getCodigo(),
					'prod_cat_idCategoria' => $categoria->getCodigo()
				));
			}
			unset($daoCategorias);

			//varia��es
			$daoProdutosVariacoes = App_Model_DAO_Produtos_Variacoes::getInstance();
			$this->getVariacoes(); //for�a o carregamento das varia��es
			$daoProdutosVariacoes->delete($daoProdutosVariacoes->getAdapter()->quoteInto('prod_var_idProduto = ?', $this->getCodigo()));
			foreach ($this->getVariacoes() as $variacao) {
				$daoProdutosVariacoes->insert(array(
					'prod_var_idProduto' => $this->getCodigo(),
					'prod_var_idVariacao' => $variacao->getCodigo(),
					'prod_var_ordem' => $this->getVariacoes()->key() + 1
				));
			}
			unset($daoProdutosVariacoes);

			//sku
			foreach ($this->getSKU() as $sku) {
				$sku->setProduto($this);
				$sku->save();
			}

			if ($trans) $this->getTable()->getAdapter()->commit();
		} catch (App_Validate_Exception $e) {
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	public function getCodigo()
	{
		return (int) $this->prod_idProduto;
	}
	
	public function setNome($value)
	{
		$this->prod_nome = (string) $value;
		return $this;
	}

	public function getNome()
	{
		return (string) $this->prod_nome;
	}

	public function setMarca(App_Model_Entity_Marca $value)
	{
		$this->objMarca = $value;
		$this->prod_idMarca = $value->getCodigo();
		return $this;
	}

	public function getMarca()
	{
		if (null == $this->objMarca && $this->getCodigo()) {
			$this->objMarca = $this->findParentRow(App_Model_DAO_Marcas::getInstance(), 'Marca');
		}
		return $this->objMarca;
	}

	
	public function setTags($value)
	{
		$this->prod_tags = (string) $value;
		return $this;
	}

	public function getTags()
	{
		return (string) $this->prod_tags;
	}

	public function setMetaTitle($value)
	{
		$this->prod_meta_title = (string) $value;
		return $this;
	}

	public function getMetaTitle()
	{
		return (string) $this->prod_meta_title;
	}

	public function setMetaTags($value)
	{
		$this->prod_meta_tags = (string) $value;
		return $this;
	}

	public function getMetaTags()
	{
		return (string) $this->prod_meta_tags;
	}

	public function setMetaDescription($value)
	{
		$this->prod_meta_description = (string) $value;
		return $this;
	}

	public function getMetaDescription()
	{
		return (string) $this->prod_meta_description;
	}

	public function setDestaqueHome($value)
	{
		$this->prod_destaqueHome = (bool) $value;
		return $this;
	}

	public function getDestaqueHome()
	{
		return (bool) $this->prod_destaqueHome;
	}

	public function setDescricao($value)
	{
		$this->prod_descricao = (string) $value;
		return $this;
	}

	public function getDescricao()
	{
		return (string) $this->prod_descricao;
	}
	
	public function setStatus($value)
	{
		$this->prod_status = (bool) $value;
		return $this;
	}

	public function getStatus()
	{
		return (bool) $this->prod_status;
	}

	public function getCategorias($ativo = false)
	{
		if (null == $this->objCategorias) {
			$select = ($ativo) ? $this->getTable()->select()->where('cat_status = ?', 1) : null;
			if ($this->getCodigo()) {
				$this->objCategorias = $this->findManyToManyRowset(
					App_Model_DAO_Categorias::getInstance(),
					App_Model_DAO_Produtos_Categorias::getInstance(),
					'Produto',
					'Categoria',
					$select
				);
			} else {
				$this->objCategorias = App_Model_DAO_Categorias::getInstance()->createRowset();
			}
		}
		return $this->objCategorias;
	}

	public function getSKU()
	{
		if (null == $this->objSKU) {
			if ($this->getCodigo()) {
				$this->objSKU = $this->findDependentRowset(App_Model_DAO_Produtos_SKU::getInstance(), 'Produto', $this->getTable()->select()->order('prod_sku_ordem ASC'));
				foreach ($this->objSKU as $sku) {
					$sku->setProduto($this);
				}
				$this->objSKU->rewind();
			} else {
				$this->objSKU = App_Model_DAO_Produtos_SKU::getInstance()->createRowset();
			}
		}
		return $this->objSKU;
	}

	public function getEstoque()
	{
		$estoque = 0;
		foreach ($this->getSKU() as $sku) {
			if(!$sku->getStatus()) continue;
			$estoque += $sku->getEstoque();
		}
		return $estoque;
	}

	public function getVitrine()
	{
		$retorno = null;
		foreach ($this->getSKU() as $sku) {
			if ($sku->getVitrine() == true) {
				$retorno = $sku;
				break;
			}
		}
		return $retorno;
	}

	public function getVariacoes()
	{
		if (null == $this->objVariacoes) {
			if ($this->getCodigo()) {
				$this->objVariacoes = $this->findManyToManyRowset(
					App_Model_DAO_Variacoes::getInstance(),
					App_Model_DAO_Produtos_Variacoes::getInstance(),
					'Produto',
					'Variacao',
					App_Model_DAO_Produtos_Variacoes::getInstance()->select()->order('prod_var_ordem ASC')
				);
			} else {
				$this->objVariacoes = App_Model_DAO_Variacoes::getInstance()->createRowset();
			}
		}
		return $this->objVariacoes;
	}
}