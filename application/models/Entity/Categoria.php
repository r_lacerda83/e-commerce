<?php

class App_Model_Entity_Categoria extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Categoria
	 */
	protected $objPai = null;
	
	/**
	 * @var App_Model_Collection of App_Model_Entity_Categoria
	 */
	protected $objChilds = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Produto
	 */
	protected $objProdutos = null;
	
	/**
	 * @var  App_Model_Collection of App_Model_Entity_Produto
	 */
	protected $objProdutosDestaque = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPai', 'objChilds', 'objProdutos'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Categorias::getInstance());
	}

	public function save()
	{
		$this->cat_status = (int) $this->cat_status;

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'cat_nome' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 50)
			),
			'cat_ordem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'cat_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());
		
		//persiste os dados no banco
		try {
			parent::save();
			
		} catch (Exception $e) {
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	/**
	 * Recupera o c�digo identificador da categoria
	 *
	 * @return integer
	 */
	public function getCodigo()
	{
		return $this->cat_idCategoria;
	}

	/**
	 * Define a categoria pai da categoria
	 *
	 * @param App_Model_Entity_Categoria $value
	 * @return App_Model_Entity_Categoria
	 */
	public function setPai(App_Model_Entity_Categoria $value = null)
	{
		$this->objPai = $value;
		$this->cat_idCategoriaPai = (null == $value ? null : $value->getCodigo());
		return $this;
	}

	/**
	 * Recupera a categoria pai da categoria
	 *
	 * @return App_Model_Entity_Categoria or NULL
	 */
	public function getPai($ativo = false)
	{
		if (null === $this->objPai && $this->getCodigo()) {	
			$select = ($ativo) ? $this->getTable()->select()->where('cat_status = ?', 1) : null;			
			$this->objPai = $this->findParentRow(App_Model_DAO_Categorias::getInstance(), 'Filho', $select);
		}
		return $this->objPai;
	}
	
	/**
	 * Define o nome da categoria
	 *
	 * @param string $value
	 * @return App_Model_Entity_Categoria
	 */
	public function setNome($value)
	{
		$this->cat_nome = (string) $value;
		return $this;
	}

	/**
	 * Recupera o nome da categoria
	 *
	 * @return string
	 */
	public function getNome()
	{
		return (string) $this->cat_nome;
	}

	/**
	 * Define a orde de exibi��o da categoria
	 *
	 * @param integer $value
	 * @return App_Model_Entity_Categoria
	 */
	public function setOrdem($value)
	{
		$this->cat_ordem = (int) $value;
		return $this;
	}

	/**
	 * Recupera a ordem de exibi��o da categoria
	 *
	 * @return integer
	 */
	public function getOrdem()
	{
		return (int) $this->cat_ordem;
	}

	/**
	 * Define o status da categoria
	 *
	 * @param boolean $value
	 * @return App_Model_Entity_Categoria
	 */
	public function setStatus($value)
	{
		$this->cat_status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera o status da categoria
	 *
	 * @return boolean
	 */
	public function getStatus()
	{
		return (bool) $this->cat_status;
	}
	
	/**
	 * Recupera as categorias filhos da categoria
	 *
	 * @return App_Model_Collection of App_Model_Entity_Categoria
	 */
	public function getChilds($todos = true)
	{
		if (null === $this->objChilds) {
			if ($this->getCodigo()) {
				$daoCategorias = App_Model_DAO_Categorias::getInstance();
				$select = $daoCategorias->select()->where('cat_idCategoriaPai = ?', $this->getCodigo())->order('cat_ordem ASC');
				
				if(!$todos) {
					$select->where('cat_status = ?', 1);
				}
				
				$this->objChilds = $daoCategorias->fetchAll($select);
				foreach ($this->objChilds as $child) {
					$child->setPai($this);
				}
				$this->objChilds->rewind();
			} else {
				$this->objChilds = $daoCategorias->createRowset();
			}
		}
		return $this->objChilds;
	}
	
	public function getChildsMarcas(App_Model_Entity_Marca $marca){
		$daoCategorias = App_Model_DAO_Categorias::getInstance();
		$daoProdutos = App_Model_DAO_Produtos::getInstance();
		$daoProdutosCategorias = App_Model_DAO_Produtos_Categorias::getInstance();
		$daoMarcas = App_Model_DAO_Marcas::getInstance();
		
		$select = $daoCategorias->select()
			->from($daoCategorias->info('name'))
			->joinInner($daoProdutosCategorias->info('name'), 'cat_idCategoria = prod_cat_idCategoria', null)
			->joinInner($daoProdutos->info('name'), 'prod_cat_idProduto = prod_idProduto', null)
			->joinInner($daoMarcas->info('name'), 'marc_idMarca = prod_idMarca', null)
			->where('marc_idMarca = ?', $marca->getCodigo())
			->where('cat_status = ?', 1)
			->where("cat_idCategoriaPai = ?", $this->getCodigo())
			->order('cat_ordem')
			->group('cat_idCategoria');		
			
		return $daoCategorias->createRowset($daoCategorias->getAdapter()->fetchAll($select));
	}

	/**
	 * Recupera os produtos relacionados a categoria
	 *
	 * @return App_Model_Collection of App_Model_Entity_Produto
	 */
	public function getProdutos()
	{
		if (null == $this->objProdutos && $this->getCodigo()) {
			$daoProdutos = App_Model_DAO_Produtos::getInstance();
			$daoProdutosCategorias = App_Model_DAO_Produtos_Categorias::getInstance();
			$this->objProdutos = $daoProdutos->fetchAll(
				$daoProdutos->select()->from($daoProdutos->info('name'))
					->joinInner($daoProdutosCategorias->info('name'), 'prod_cat_idProduto = prod_idProduto', null)
				->where('prod_cat_idCategoria = ?', $this->getCodigo())
			);
			unset($daoProdutos, $daoProdutosCategorias);
		}
		return $this->objProdutos;
	}

	public function countProdutos($filter = null)
	{
		$daoProdutos = App_Model_DAO_Produtos::getInstance();
		$daoProdutosCategorias = App_Model_DAO_Produtos_Categorias::getInstance();

		$select = $daoProdutos->getAdapter()->select()
			->from(array('produtos' => $daoProdutos->buscaProduto()))
			->joinInner($daoProdutosCategorias->info('name'), 'prod_cat_idProduto = prod_idProduto', null)
			->where('prod_status = ?', 1)
			->where('prod_cat_idCategoria = ?', $this->getCodigo());
		if (null != $filter) {
			$select->where('prod_nome LIKE ?', "%{$filter}%");
		}
		$select = $daoProdutos->getAdapter()->select()->from($select, 'COUNT(1)');

		return $daoProdutos->getAdapter()->fetchOne($select);
	}
}