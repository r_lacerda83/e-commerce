<?php

class App_Model_Entity_Pedido_Item_Produto extends App_Model_Entity_Pedido_Item_Abstract
{
	/**
	 * @var App_Model_Entity_Produto_SKU
	 */
	protected $objSKU;

	protected $presente = false;
	protected $presenteValor;

	/**
	 * Construtor
	 *
	 * @param App_Model_Entity_Produto $sku
	 * @return App_Model_Entity_Pedido_Item_Produto
	 */
	public function __construct(App_Model_Entity_Produto_SKU $sku)
	{
		$this->objSKU = $sku;
	}

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objSKU'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Items::getInstance());
	}

	public function getCodigo()
	{
		return (int) $this->objSKU->getProduto()->getCodigo();
	}

	public function getNome()
	{
		return "{$this->objSKU->getProduto()->getNome()} - {$this->objSKU->getNome()}";
	}

	public function getPeso()
	{
		return number_format($this->objSKU->getPeso() * $this->quantidade, 2, '.', '');
	}

	public function setPresente($value)
	{
		if ($this->objSKU->getPresente() === true)
		{
			$this->presente = (bool) $value;
		}
		return $this;
	}

	public function getPresente()
	{
		return $this->presente;
	}

	/**
	 * Retorna se o valor embalagem do presente
	 *
	 */
	public function getPresenteValor()
	{

		return $this->presenteValor;

	}

	public function setPresenteValor($value)
	{
		if ($this->objSKU->getPresente() === true)
		{
			$this->presenteValor =  $value;
		}
		return $this;
	}

	public function getProduto()
	{
		return $this->objSKU;
	}
}