<?php

abstract class App_Model_Entity_Pedido_Item_Abstract extends App_Model_Listener
{
	const ON_CHANGE_QUANTIDADE = 'changeQuantidade';

	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $pedido;

	protected $quantidade = 1;
	protected $brinde = false;

	abstract public function getCodigo();

	abstract public function getNome();

	abstract public function getValor();

	abstract public function getPeso();

	abstract public function getProduto();

	/**
	 * Define a qual pedido o item pertence
	 *
	 * @param App_Model_Entity_Pedido $value
	 * @return App_Model_Entity_Pedido_Item_Abstract
	 */
	public function setPedido(App_Model_Entity_Pedido $value)
	{
		$this->pedido = $value;
		return $this;
	}

	/**
	 * Recupera o pedido a qual o item pertence
	 *
	 * @return App_Model_Entity_Pedido
	 */
	public function getPedido()
	{
		return $this->pedido;
	}

	/**
	 * Define a quantidade do item
	 *
	 * @param integer $value
	 * @return App_Model_Entity_Pedido_Item_Abstract
	 */
	public function setQuantidade($value)
	{
		if (false !== $this->fireEvent(self::ON_CHANGE_QUANTIDADE, array($value))) {
			$this->quantidade = (int) $value;
			if (null !== $this->getPedido()) {
				$this->getPedido()->recalculaEntrega();
			}
		}
		return $this;
	}

	/**
	 * Recupera a quantidade do item
	 *
	 * @return integer
	 */
	public function getQuantidade()
	{
		return (int) $this->quantidade;
	}

	/**
	 * Define se o produto � um brinde
	 *
	 * @param boolean $value
	 * @return App_Model_Entity_Pedido_Item_Abstract
	 */
	public function setBrinde($value)
	{
		$this->brinde = (bool) $value;
		return $this;
	}

	/**
	 * Recupera se o produto � um brinde
	 *
	 * @return boolean
	 */
	public function getBrinde()
	{
		return (bool) $this->brinde;
	}
}