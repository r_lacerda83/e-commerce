<?php

class App_Model_Entity_Pedido_Items extends App_Model_Collection 
{
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $objPedido;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPedido'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Items::getInstance());
	}

	public function setPedido(App_Model_Entity_Pedido $value)
	{
		$this->objPedido = $value;
		return $this;
	}

	/**
	 * Retorna o pedido a qual os itens pertencem
	 *
	 * @return App_Model_Entity_Pedido
	 */
	public function getPedido()
	{
		return $this->objPedido;
	}

	/**
	 * Adiciona um item ao pedido
	 *
	 * @param App_Model_Entity_Pedido_Item_Abstract $item
	 * @return App_Model_Entity_Pedido_Items
	 * @throws Exception
	 */
	public function add(App_Model_Entity_Pedido_Item $item)
	{
		$item->setPedido($this->getPedido());
		parent::offsetAdd($item);		
		return $this;
	}

	/**
	 * Remove um item do pedido
	 *
	 * @param App_Model_Entity_Pedido_Item $item
	 * @return App_Model_Entity_Pedido_Items
	 */
	public function remove(App_Model_Entity_Pedido_Item $item)
	{
		foreach ($this->_data as $offset => $produto) {
			if ($item->getProduto()->getSKU() == $produto['ped_item_sku']) {
				parent::offsetUnset($offset);
				break;
			}
		}
		return $this;
	}

	/**
	 * Calcula o valor dos itens do pedido
	 *
	 * @param boolean $descontos Incluir valores de descontos
	 * @return float
	 */
	public function getValor()
	{
		$valor = 0;
		$lista = clone $this;
		foreach ($lista as $item) {
			$valor += $item->getValor();
		}

		return $valor;
	}

	/**
	 * Calcula o peso dos itens do pedido
	 *
	 * @return integer
	 */
	public function getPeso()
	{
		$peso = 0;
		$key = $this->key();
		foreach ($this as $item) {
			$peso += $item->getPeso();
		}
		$this->seek($key);
		return $peso;
	}

	/**
	 * Retorna a quantidade de itens no carrinho
	 * 
	 * @param boolean $brindes Incluir produtos de brinde na contagem
	 * @return integer
	 */
	public function count()
	{
		return parent::count();
	}
	
}