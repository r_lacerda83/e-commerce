<?php

final class App_Model_Entity_Pedido_Entrega_Forma_Correios extends App_Model_Entity_Pedido_Entrega_Abstract
{
	/**
	 * @var Zend_Config_Xml
	 */
	private $config;

	/**
	 * @var Zend_Cache
	 */
	private $cache;

	protected $codigo = 'C';
	protected $nome = 'Correios';

	/**
	 * Construtor
	 *
	 * @param App_Model_Entity_Pedido_Entrega $entrega
	 * @return App_Model_Entity_Pedido_Entrega_Forma_Correios
	 */
	public function __construct(App_Model_Entity_Pedido_Entrega $entrega)
	{
		parent::__construct($entrega);
		$this->config = new Zend_Config_Xml(APPLICATION_ROOT.'/configs/correios.xml');

		$frontendOptions = array(
			'lifetime' => 18000, //5 horas
			'automatic_serialization' => true
		);
		$backendOptions = array(
			'cache_dir' => APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'correios'
		);
		$this->cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$this->cache->remove(Zend_Cache::CLEANING_MODE_OLD);
	}

	public function __sleep()
	{
		return array_merge(parent::__sleep(), array('config', 'cache'));
	}

	/**
	 * (non-PHPdoc)
	 * @see App_Model_Entity_Pedido_Entrega_Forma#getServico($codigo)
	 */
	public function getServico($codigo)
	{
		foreach ($this->getServicosDisponiveis() as $servico) {
			if ($servico->getCodigo() == $codigo) {
				return $servico;
			}
		}
		throw new Exception("C�digo {$codigo} n�o � um servi�o dos Correios v�lido.");
	}

	/**
	 * (non-PHPdoc)
	 * @see App_Model_Entity_Pedido_Entrega_Forma#getServicosDisponiveis()
	 */
	public function getServicosDisponiveis()
	{
		$retorno = new ArrayObject();
		foreach ($this->config->services->service as $servico) {
			$objServico = new App_Model_Entity_Pedido_Entrega_Servico($this);
			$objServico->setCodigo($servico->code)
				->setNome($servico->name)
				->setDescricao($servico->description)
				->setStatus($servico->enabled);

			$retorno->append($objServico);
			unset($objServico);
		}

		if (($this->objEntrega->getPedido() != null && !$this->objEntrega->getPedido()->getCodigo()) || !$this->objEntrega->ped_ent_idPedido) {
			$this->calcula($retorno);
		}
		return $retorno;
	}

	protected function calcula(ArrayObject $servicos)
	{	
		foreach ($servicos as $servico) {
			
			$valorDeclarado = 0;
			$peso = 0;

			if ($servico->getStatus() == false) {
				continue;
			}

			if ($this->getEntrega()->getPedido()) {
				foreach ($this->getEntrega()->getPedido()->getItems() as $item) {
					$valorDeclarado += $item->getValor();
					$peso += $item->getPeso();
				}
			}
			
			if(APPLICATION_ENV == 'production') {
			
				$cepDestino = preg_replace('/[^0-9]/', '', $this->getEntrega()->getEndereco()->getCep());
				$cepOrigem = preg_replace('/[^0-9]/', '', $this->config->cepOrigem);
				
				$cacheID = eregi_replace('[^0-9|a-z|A-Z|_]', '', "{$cepDestino}_{$valorDeclarado}_{$peso}_{$servico->getCodigo()}");
				if (!($dataCache = $this->cache->load($cacheID))) {
					$peso = number_format($peso, 2, ',', '');
					$valorDeclarado = number_format($valorDeclarado, 2, ',', '');
				
					$url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa={$this->config->empresa}&sDsSenha={$this->config->senha}&sCepOrigem={$cepOrigem}&sCepDestino={$cepDestino}&nVlPeso={$peso}&nCdFormato={$this->config->formato}&nVlComprimento=21&nVlAltura=14&nVlLargura=11&sCdMaoPropria={$this->config->maoPropria}&nVlValorDeclarado={$valorDeclarado}&sCdAvisoRecebimento={$this->config->avisoRecebimento}&nCdServico={$servico->getCodigo()}&nVlDiametro=0&StrRetorno=xml";
					$xml = simplexml_load_file($url);
					$arrayRetorno = array();
					$count = 0;
					
					foreach( $xml->cServico as $servicoCorreio ) {
						if($servicoCorreio->Erro == 0) {
							$servico->setValor(0)->setStatus(true);
							$this->cache->save(array('valor' => 0, 'status' => true), $cacheID);
						} else {
							$servico->setStatus(false);
							$this->cache->save(array('valor' => 0, 'status' => false), $cacheID);
						}
					}					
					
				} else {
					$servico->setValor($dataCache['valor']);
					$servico->setStatus($dataCache['status']);
				}
				
			} else {
				if($valorDeclarado > 0) {
					
					$servico->setValor(1.00);
					$servico->setDescricao("(Teste) Prazo de 10 (dias �teis)");
					
				} else {
					
					$servico->setValor(0)->setStatus(true);
				}
			}
			
		}
	}
}