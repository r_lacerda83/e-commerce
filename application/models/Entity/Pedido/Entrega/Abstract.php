<?php

abstract class App_Model_Entity_Pedido_Entrega_Abstract
{
	/**
	 * @var App_Model_Entity_Pedido_Entrega
	 */
	protected $objEntrega;

	protected $codigo;
	protected $nome;

	public function __construct(App_Model_Entity_Pedido_Entrega $entrega)
	{
		$this->objEntrega = $entrega;
	}

	public function __sleep()
	{
		$fields = array('objEntrega');
		return $fields;
	}

	public function getCodigo()
	{
		return $this->codigo;
	}

	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Recupera a entrega desta forma
	 *
	 * @return App_Model_Entity_Pedido_Entrega
	 */
	public function getEntrega()
	{
		return $this->objEntrega;
	}

	/**
	 * Recupera um servi�o pelo c�digo
	 *
	 * @param string $codigo
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	abstract public function getServico($codigo);

	/**
	 * Recupera a lista de servi�os dispon�veis
	 *
	 * @param App_Model_Entity_Pedido $pedido
	 * @return App_Model_Collection of App_Model_Entity_Pedido_Entrega_Servico
	 */
	abstract public function getServicosDisponiveis();
}