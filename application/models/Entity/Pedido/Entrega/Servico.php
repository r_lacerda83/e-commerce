<?php

class App_Model_Entity_Pedido_Entrega_Servico
{
	/**
	 * @var App_Model_Entity_Pedido_Entrega_Abstract
	 */
	protected $forma;

	protected $codigo;
	protected $nome;
	protected $descricao;
	protected $valor;
	protected $codigo_rastreamento;
	protected $status;

	/**
	 * Construtor
	 *
	 * @param App_Model_Entity_Pedido_Entrega_Abstract $forma
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	public function __construct(App_Model_Entity_Pedido_Entrega_Abstract $forma)
	{
		$this->forma = $forma;
	}

	/**
	 * Recupera a forma de entrega do servi�o
	 *
	 * @return App_Model_Entity_Pedido_Entrega_Abstract
	 */
	public function getForma()
	{
		return $this->forma;
	}

	/**
	 * Define o c�digo de identifica��o do servi�o
	 *
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	public function setCodigo($value)
	{
		$this->codigo = $value;
		return $this;
	}

	/**
	 * Recupera o c�digo de identifica��o do servi�o
	 *
	 * @return string
	 */
	public function getCodigo()
	{
		return $this->codigo;
	}

	/**
	 * Define o nome do servi�o
	 *
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	public function setNome($value)
	{
		$this->nome = $value;
		return $this;
	}

	/**
	 * Recupera o nome do servi�o
	 *
	 * @return string
	 */
	public function getNome()
	{
		return $this->nome;
	}

	/**
	 * Define a descri��o do servi�o
	 *
	 * @param string $value
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	public function setDescricao($value)
	{
		$this->descricao = (string) $value;
		return $this;
	}

	/**
	 * Recupera a descri��o do servi�o
	 *
	 * @return string
	 */
	public function getDescricao()
	{
		return $this->descricao;
	}

	/**
	 * Define o valor do servi�o
	 *
	 * @param float $value
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	public function setValor($value)
	{
		$this->valor = (float) $value;
		return $this;
	}

	/**
	 * Recupera o valor do servi�o
	 *
	 * @return float
	 */
	public function getValor()
	{
		return (float) $this->valor;
	}

	public function setCodigoRastreamento($value)
	{
		$this->codigo_rastreamento = (string) $value;
		return $this;
	}

	public function getCodigoRastreamento()
	{
		return (string) $this->codigo_rastreamento;
	}

	/**
	 * Define o status do servi�o
	 *
	 * @param boolean $value
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	public function setStatus($value)
	{
		$this->status = (bool) $value;
		return $this;
	}

	/**
	 * Recupera o status do servi�o
	 *
	 * @return boolean $value
	 */
	public function getStatus()
	{
		return (bool) $this->status;
	}
}