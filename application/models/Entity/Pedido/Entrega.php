<?php

class App_Model_Entity_Pedido_Entrega extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $objPedido = null;

	/**
	 * @var App_Model_Endereco
	 */
	protected $objEndereco = null;

	/**
	 * @var App_Model_Entity_Pedido_Entrega_Servico
	 */
	protected $objServico = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Pedido_Entrega_Servico
	 */
	protected $objFormas = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPedido', 'objEndereco', 'objServico', 'objFormas'));
		return $fields;
	}

	public function __wakeup()
	{
		$this->init();
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Entrega::getInstance());
	}

	public function init()
	{
		$this->objEndereco = new App_Model_Endereco($this, array(
			App_Model_Endereco::CEP => 'ped_ent_cep',
			App_Model_Endereco::LOGRADOURO => 'ped_ent_logradouro',
			App_Model_Endereco::NUMERO => 'ped_ent_numero',
			App_Model_Endereco::COMPLEMENTO => 'ped_ent_complemento',
			App_Model_Endereco::BAIRRO => 'ped_ent_bairro',
			App_Model_Endereco::CIDADE => 'ped_ent_cidade',
			App_Model_Endereco::UF => 'ped_ent_uf'
		));
	}

	public function save()
	{
		$this->ped_ent_idPedido = $this->getPedido()->getCodigo();
		if (null != $this->getServico()) {
			$this->ped_ent_forma = $this->getServico()->getForma()->getCodigo();
			$this->ped_ent_servico = $this->getServico()->getCodigo();
			$this->ped_ent_valor = $this->getServico()->getValor();
			$this->ped_ent_codigo_rastreamento = $this->getServico()->getCodigoRastreamento();
			/**
			 * TODO: ver se � poss�vel salvar a regra de frete gr�tis
			 */
		}

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_ent_logradouro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 160)
			),
			'ped_ent_numero' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 20)
			),
			'ped_ent_complemento' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1, 40)
			),
			'ped_ent_cep' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(9, 9)
			),
			'ped_ent_bairro' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 60)
			),
			'ped_ent_cidade' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 60)
			),
			'ped_ent_uf' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(2, 2)
			),
			'ped_ent_idFrete' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'ped_ent_forma' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 1)
			),
			'ped_ent_servico' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false,
				new Zend_Validate_StringLength(1, 10)
			),
			'ped_ent_valor' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'ped_ent_codigo_rastreamento' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true,
				new Zend_Validate_StringLength(1, 30)
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		parent::save();
	}

	public function setPedido(App_Model_Entity_Pedido $value)
	{
		$this->objPedido = $value;
		$this->ped_ent_idPedido = $value->getCodigo();
		return $this;
	}

	public function getPedido()
	{
		if (null == $this->objPedido && $this->ped_ent_idPedido) {
			$this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
		}
		return $this->objPedido;
	}

	/**
	 * Recupera o endere�o de entrega do pedido
	 * 
	 * @return App_Model_Endereco
	 */
	public function getEndereco()
	{
		return $this->objEndereco;
	}

	/**
	 * Define o servi�o de entrega
	 *
	 * @param App_Model_Entity_Pedido_Entrega_Servico $value
	 * @return App_Model_Entity_Pedido_Entrega
	 */
	public function setServico(App_Model_Entity_Pedido_Entrega_Servico $value = null)
	{
		$this->objServico = $value;
		$this->ped_ent_servico = ($value ? $value->getCodigo() : null);
		return $this;
	}

	/**
	 * Recupera o servi�o de entrega
	 *
	 * @return App_Model_Entity_Pedido_Entrega_Servico
	 */
	public function getServico()
	{
		if (null == $this->objServico && $this->ped_ent_idPedido && ($this->ped_ent_forma && $this->ped_ent_servico)) {
			foreach ($this->getFormasDisponiveis() as $forma) {
				if ($forma->getCodigo() == $this->ped_ent_forma) {
					$servico = $forma->getServico($this->ped_ent_servico);
					$servico->setValor($this->ped_ent_valor)
						->setCodigoRastreamento($this->ped_ent_codigo_rastreamento);
					$this->objServico = $servico;
				}
			}
		}
		return $this->objServico;
	}

	public function getFormasDisponiveis()
	{
		if (null == $this->objFormas) {
			$this->objFormas = new ArrayObject();

			//varre o diret�rio Entrega/Forma procurando as classes dispon�veis
			$path = sprintf('%s%s%s%s%s', dirname(__FILE__), DIRECTORY_SEPARATOR, 'Entrega', DIRECTORY_SEPARATOR, 'Forma');
			$classes = new DirectoryIterator($path);
			foreach ($classes as $file) {
				if ($file->isDot() || $file->isDir()) {
					continue;
				}

				$classname = "App_Model_Entity_Pedido_Entrega_Forma_{$file->getBasename('.php')}";
				if (class_exists($classname)) {
					$class = new $classname($this);
					$this->objFormas->append($class);
				}
			}
		}
		return $this->objFormas;
	}
	
	public function verificaDisponibilidade()
	{
		return true;
	}
}