<?php

class App_Model_Entity_Pedido_Status extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $objPedido = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objPedido'));
		return $fields;
	}

	public function __wakeup()
	{
		$this->init();
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos_Status::getInstance());
	}

	public function save()
	{
		parent::save();
	}

	public function getCodigo()
	{
		return (int) $this->ped_status_idStatus;
	}

	public function setPedido(App_Model_Entity_Pedido $value)
	{
		$this->objPedido = $value;
		$this->ped_status_idPedido = $value->getCodigo();
		return $this;
	}

	public function getPedido()
	{
		if (null == $this->objPedido && $this->ped_ent_idPedido) {
			$this->objPedido = $this->findParentRow(App_Model_DAO_Pedidos::getInstance(), 'Pedido');
		}
		return $this->objPedido;
	}

	public function setValor($value)
	{
		$this->ped_status_valor = (int) $value;
		return $this;
	}

	public function getValor()
	{
		return (int) $this->ped_status_valor;
	}

	public function setData($value)
	{
		$this->ped_status_data = (string) $value;
		return $this;
	}

	public function getData()
	{
		return (string) $this->ped_status_data;
	}

	public function setDescricao($value)
	{
		$this->ped_status_descricao = (string) $value;
		return $this;
	}

	public function getDescricao()
	{
		return (string) $this->ped_status_descricao;
	}
}