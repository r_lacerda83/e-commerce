<?php

class App_Model_Entity_Produto_SKU extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Produto
	 */
	protected $objProduto = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Variacao_Atributo
	 */
	protected $objAtributos = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagens = null;

	
	protected $avisoDisponibilidade = false;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objProduto', 'objAtributos', 'objImagens'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Produtos_SKU::getInstance());
	}

	public function save()
	{
		$this->prod_sku_vitrine = (int) $this->prod_sku_vitrine;
		$this->prod_sku_presente = (int) $this->prod_sku_presente;
		$this->prod_sku_ordem = (int) $this->prod_sku_ordem;
		$this->prod_sku_status = (int) $this->prod_sku_status;

		foreach ($this->_data as $field => $value) {
			$this->{$field} = $value;
		}

		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'prod_sku_idProduto' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_preco' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_refFornecedor' => array(
				Zend_Filter_Input::ALLOW_EMPTY => true
			),
			'prod_sku_custo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_peso' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_altura' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_largura' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_comprimento' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_estoque' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_estoqueMinimo' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_presente' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_ordem' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			),
			'prod_sku_status' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consist�ncia dos dados
		$this->validate($filters, $validators, $this->toArray());

		//persiste os dados no banco
		try {
			$this->getTable()->getAdapter()->beginTransaction();
			$trans = true;
		} catch (Exception $e) {
			$trans = false;
		}
		try {
			parent::save();

			//atributos
			$daoAtributos = App_Model_DAO_Produtos_SKU_Atributos::getInstance();
			$this->getAtributos(); //for�a o carregamento dos atributos
			$daoAtributos->delete($daoAtributos->getAdapter()->quoteInto('prod_sku_atr_SKU = ?', $this->getSKU()));
			foreach ($this->getAtributos() as $atributo) {
				$daoAtributos->insert(array(
					'prod_sku_atr_SKU' => $this->getSKU(),
					'prod_sku_atr_idAtributo' => $atributo->getCodigo()
				));
			}
			unset($daoAtributos);

			//imagens
			$daoImagens = App_Model_DAO_Produtos_SKU_Imagens::getInstance();
			$this->getImagens(); //for�a o carregamento das imagens
			$daoImagens->delete($daoImagens->getAdapter()->quoteInto('prod_sku_img_SKU = ?', $this->getSKU()));
			foreach ($this->getImagens() as $imagem) {
				$daoImagens->insert(array(
					'prod_sku_img_SKU' => $this->getSKU(),
					'prod_sku_img_idImagem' => $imagem->getCodigo(),
					'prod_sku_img_ordem' => $this->getImagens()->key() + 1
				));
			}
			unset($daoImagens);

			if ($trans) $this->getTable()->getAdapter()->commit();
		} catch (Exception $e) {
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	public function delete()
	{
		$this->getTable()->getAdapter()->beginTransaction();
		try {
			$produto = $this->getProduto();
			parent::delete();

			if (null == $produto->getVitrine()) {
				$produto->getSKU()->rewind();
				$produto->getSKU()->current()->setVitrine(true)->save();
			}

			$this->getTable()->getAdapter()->commit();
		} catch (Exception $e) {
			$this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}

	public function setSKU($value)
	{
		$this->prod_sku_SKU = (string) $value;
		return $this;
	}

	public function getSKU()
	{
		return (string) $this->prod_sku_SKU;
	}

	public function setProduto(App_Model_Entity_Produto $value)
	{
		$this->objProduto = $value;
		$this->prod_sku_idProduto = $value->getCodigo();
		return $this;
	}

	public function getProduto()
	{
		if (null == $this->objProduto && $this->getSKU()) {
			$this->objProduto = $this->findParentRow(App_Model_DAO_Produtos::getInstance(), 'Produto');
		}
		return $this->objProduto;
	}

	public function getNome($product = true, $separator = ' - ')
	{
		$items = array();		
		foreach ($this->getAtributos() as $atributo) {
			//$items[] = "{$atributo->getVariacao()->getRotulo()}: {$atributo->getValor()}";
			$items[] = "{$atributo->getValor()}";
		}
		$nome = implode($separator, $items);

		if ($product == true) {
			//$nome = "{$this->getProduto()->getNome()} {$separator} {$nome}";
			$nome = "{$this->getProduto()->getNome()} {$nome}";
		}

		return $nome;
	}

	public function setVitrine($value)
	{
		$this->prod_sku_vitrine = (bool) $value;
		return $this;
	}

	public function getVitrine()
	{
		return (bool) $this->prod_sku_vitrine;
	}
	
	public function setRefFornecedor($value)
	{
		$this->prod_sku_refFornecedor = (string) $value;
		return $this;
	}

	public function getRefFornecedor()
	{
		return (string) $this->prod_sku_refFornecedor;
	}

	public function setPreco($value)
	{
		$this->prod_sku_preco = (float) $value;
		return $this;
	}

	public function getPreco()
	{
		return (float) $this->prod_sku_preco;
	}

	public function getPrecoOriginal()
	{
		return (float) $this->prod_sku_preco;
	}

	public function setCusto($value)
	{
		$this->prod_sku_custo = (float) $value;
		return $this;
	}

	public function getCusto()
	{
		return (float) $this->prod_sku_custo;
	}

	public function setPeso($value)
	{
		$this->prod_sku_peso = (float) $value;
		return $this;
	}

	public function getPeso()
	{
		return (float) $this->prod_sku_peso;
	}

	public function setAltura($value)
	{
		$this->prod_sku_altura = (float) $value;
		return $this;
	}

	public function getAltura()
	{
		return (float) $this->prod_sku_altura;
	}

	public function setLargura($value)
	{
		$this->prod_sku_largura = (float) $value;
		return $this;
	}

	public function getLargura()
	{
		return (float) $this->prod_sku_largura;
	}

	public function setComprimento($value)
	{
		$this->prod_sku_comprimento = (float) $value;
		return $this;
	}

	public function getComprimento()
	{
		return (float) $this->prod_sku_comprimento;
	}

	public function setEstoque($value)
	{
		$this->prod_sku_estoque = (int) $value;
		return $this;
	}

	public function getEstoque()
	{
		return (int) $this->prod_sku_estoque;
	}

	public function setEstoqueMinimo($value)
	{
		$this->prod_sku_estoqueMinimo = (int) $value;
		return $this;
	}

	public function getEstoqueMinimo()
	{
		return (int) $this->prod_sku_estoqueMinimo;
	}

	public function setPresente($value)
	{
		$this->prod_sku_presente = (bool) $value;
		return $this;
	}

	public function getPresente()
	{
		return (bool) $this->prod_sku_presente;
	}

	public function setPresentePreco($value)
	{
		$this->prod_sku_presentePreco = (float) $value;
		return $this;
	}

	public function getPresentePreco()
	{
		return (float) $this->prod_sku_presentePreco;
	}

	public function setOrdem($value)
	{
		$this->prod_sku_ordem = (int) $value;
		return $this;
	}

	public function getOrdem()
	{
		return (int) $this->prod_sku_ordem;
	}

	public function setStatus($value)
	{
		$this->prod_sku_status = (bool) $value;
		return $this;
	}

	public function getStatus()
	{
		return (bool) $this->prod_sku_status;
	}

	public function getAtributos()
	{
		if (null == $this->objAtributos) {
			if ($this->getSKU()) {
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$daoProdutosSku = App_Model_DAO_Produtos_SKU::getInstance();
				$daoProdutosSkuAtributos = App_Model_DAO_Produtos_SKU_Atributos::getInstance();
				$daoProdutosVariacoes = App_Model_DAO_Produtos_Variacoes::getInstance();
				$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
				$daoVariacoesAtributos = App_Model_DAO_Variacoes_Atributos::getInstance();

				$select = $daoProdutos->getAdapter()->select()
					->from($daoProdutosSkuAtributos->info('name'), null)
					->joinInner($daoProdutosSku->info('name'), 'prod_sku_SKU = prod_sku_atr_SKU', null)
					->joinInner($daoVariacoesAtributos->info('name'), 'var_atr_idAtributo = prod_sku_atr_idAtributo')
					->joinInner($daoVariacoes->info('name'), 'var_idVariacao = var_atr_idVariacao', null)
					->joinInner($daoProdutos->info('name'), 'prod_idProduto = prod_sku_idProduto', null)
					->joinInner($daoProdutosVariacoes->info('name'), 'prod_var_idVariacao = var_idVariacao AND prod_var_idProduto = prod_idProduto', null)
					->where('prod_sku_atr_SKU = ?', $this->getSKU())
					->order('prod_var_ordem ASC');

				$this->objAtributos = $daoVariacoesAtributos->createRowset(
					$daoVariacoesAtributos->getAdapter()->fetchAll($select, null, Zend_Db::FETCH_ASSOC)
				);
				
			} else {
				$this->objAtributos = App_Model_DAO_Variacoes_Atributos::getInstance()->createRowset();
			}
		}
		return $this->objAtributos;
	}

	public function getImagens()
	{
		if (null == $this->objImagens) {
			if ($this->getSKU()) {
				$this->objImagens = $this->findManyToManyRowset(
					App_Model_DAO_Galerias_Arquivos::getInstance(),
					App_Model_DAO_Produtos_SKU_Imagens::getInstance(),
					'SKU',
					'Imagem',
					App_Model_DAO_Produtos_SKU_Imagens::getInstance()->select()->order('prod_sku_img_ordem ASC')
				);
			} else {
				$this->objImagens = App_Model_DAO_Galerias_Arquivos::getInstance()->createRowset();
			}
		}
		return $this->objImagens;
	}
}