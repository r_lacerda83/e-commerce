<?php

class App_Model_Entity_Variacao_Atributo extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Variacao
	 */
	protected $objVariacao = null;

	/**
	 * @var App_Model_Entity_Galeria_Arquivo
	 */
	protected $objImagem = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objVariacao', 'objImagem'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Variacoes_Atributos::getInstance());
	}

	public function save()
	{
		$this->var_atr_status = (int) $this->var_atr_status;
		parent::save();
	}

	/**
	 * Recupera o c�digo identificador da varia��o
	 *
	 * @return integer
	 */
	public function getCodigo()
	{
		return (int) $this->var_atr_idAtributo;
	}

	public function setVariacao(App_Model_Entity_Variacao $value)
	{
		$this->objVariacao = $value;
		$this->var_atr_idVariacao = $value->getCodigo();
		return $this;
	}

	public function getVariacao()
	{
		if (null == $this->objVariacao && $this->getCodigo()) {
			$this->objVariacao = $this->findParentRow(App_Model_DAO_Variacoes::getInstance(), 'Variacao');
		}
		return $this->objVariacao;
	}

	public function setImagem(App_Model_Entity_Galeria_Arquivo $value = null)
	{
		$this->objImagem = $value;
		$this->var_atr_idImagem = ($value != null ? $value->getCodigo() : null);
		return $this;
	}

	public function getImagem()
	{
		if (null == $this->objImagem && $this->getCodigo()) {
			$this->objImagem = $this->findParentRow(App_Model_DAO_Galerias_Arquivos::getInstance(), 'Imagem');
		}
		return $this->objImagem;
	}

	public function setValor($value)
	{
		$this->var_atr_valor = (string) $value;
		return $this;
	}

	public function getValor()
	{
		return (string) $this->var_atr_valor;
	}

	public function setOrdem($value)
	{
		$this->var_atr_ordem = (int) $value;
		return $this;
	}

	public function getOrdem()
	{
		return (int) $this->var_atr_ordem;
	}

	public function setStatus($value)
	{
		$this->var_atr_status = (bool) $value;
		return $this;
	}

	public function getStatus()
	{
		return (bool) $this->var_atr_status;
	}
	
	public function isUnique() {
		return (bool) $this->var_atr_unica;
	}
}