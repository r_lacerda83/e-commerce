<?php

class App_Model_Entity_Pedido extends App_Model_Entity_Abstract
{
	/**
	 * @var App_Model_Entity_Pedido_Items
	 */
	protected $objItems = null;

	/**
	 * @var App_Model_Entity_Pedido_Entrega
	 */
	protected $objEntrega = null;

	/**
	 * @var App_Model_Entity_Cliente
	 */
	protected $objCliente = null;

	/**
	 * @var App_Model_Collection of App_Model_Entity_Pedido_Status
	 */
	protected $objStatus = null;

	public function __sleep()
	{
		$fields = array_merge(parent::__sleep(), array('objItems', 'objEntrega', 'objCliente', 'objStatus'));
		return $fields;
	}

	public function __wakeup()
	{
		parent::__wakeup();
		$this->setTable(App_Model_DAO_Pedidos::getInstance());
	}

	public function save($trans = true)
	{
		$inserting = ($this->getCodigo() ? false : true);
		if ($inserting) {
			$this->ped_data = date('Y-m-d H:i:s');
		}
		
		$filters = array(
			'*' => new Zend_Filter_StringTrim()
		);

		$validators = array(
			'ped_idCliente' => array(
				Zend_Filter_Input::ALLOW_EMPTY => false
			)
		);

		//verifica a consistência dos dados
		$this->validate($filters, $validators, $this->toArray());

		if($trans) {
			try {
				$this->getTable()->getAdapter()->beginTransaction();
				$trans = true;
			} catch (Exception $e) {
				$trans = false;
			}
		}
		
		try {
			parent::save();			

			//entrega
			$this->getEntrega()->ped_ent_idPedido = $this->getCodigo();
			$this->getEntrega()->save();

			//itens do pedido
			$this->getItems()->rewind();
			foreach ($this->getItems() as $item) {
				$item->setPedido($this);
				$item->save();
				$item->getProduto()->refresh();

				$statusAlterado = false;
				if ($inserting == false) {
					//verifica se houve alteração de status
					$daoPedidosStatus = App_Model_DAO_Pedidos_Status::getInstance();
					$totalStatus = $daoPedidosStatus->getAdapter()->fetchOne($daoPedidosStatus->select()->from($daoPedidosStatus, 'COUNT(1)')->where('ped_status_idPedido = ?', $this->getCodigo()));
					$statusAntigo = $daoPedidosStatus->fetchRow($daoPedidosStatus->select()->where('ped_status_idPedido = ?', $this->getCodigo())->order('ped_status_idStatus DESC')->limit(1));
					$statusAlterado = (bool) ($this->getHistoricoStatus()->count() != $totalStatus);
					unset($daoPedidosStatus, $totalStatus);
				}
				
				if ($inserting || ($statusAlterado && in_array($statusAntigo->getValor(), array(5,10)) && !in_array($this->getStatus()->getValor(), array(5,10)))) {
					//inserindo um novo pedido ou reativando um pedido cancelado ou abandonado, debitar estoque
					if ($item->getProduto()->getEstoque() >= $item->getQuantidade()) {
						$item->getProduto()->setEstoque($item->getProduto()->getEstoque() - $item->getQuantidade());
						$item->getProduto()->save();
					} else {
						throw new Exception("Quantidade indisponível em estoque para o produto {$item->getProduto()->getNome()}.");
					}
				} elseif ($statusAlterado && !in_array($statusAntigo->getValor(), array(5,10)) && in_array($this->getStatus()->getValor(), array(5,10))) {
					//cancelando ou abandonando o pedido, credita o estoque
					$item->getProduto()->setEstoque($item->getProduto()->getEstoque() + $item->getQuantidade());
					$item->getProduto()->save();
				}
			}
				
			//atualiza os status dos pedido
			if ($inserting == true) {			
				$this->getHistoricoStatus()->rewind();	
				
				$this->getHistoricoStatus()->offsetAdd(
					App_Model_DAO_Pedidos_Status::getInstance()->createRow(array(
						'ped_status_idPedido' => $this->getCodigo(),
						'ped_status_valor' => 1,
						'ped_status_data' => date('Y-m-d H:i:s'),
						'ped_status_descricao' => 'Pedido iniciado'
					))
				);	
			}
			
			$this->getHistoricoStatus()->rewind();
			foreach ($this->getHistoricoStatus() as $status) {
				$status->setPedido($this);
				$status->save();
			}			

			if ($trans) $this->getTable()->getAdapter()->commit();
		} catch (App_Validate_Exception $e) {
			$this->_cleanData = null;
			$this->getEntrega()->ped_ent_idPedido = null;
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new App_Validate_Exception($e->getMessage(), $e->getCode(), $e->getFields());
		} catch (Exception $e) {
			$this->_cleanData = null;
			$this->getEntrega()->ped_ent_idPedido = null;
			if ($trans) $this->getTable()->getAdapter()->rollBack();
			throw new Zend_Db_Table_Row_Exception($e->getMessage(), $e->getCode());
		}
	}
	
	public function setCodigo($value)
	{
		$this->ped_idPedido = $value != null ? (double) $value : null;
		return $this;
	}

	/**
	 * Recupera o código identificador do Pedido
	 * 
	 * @return integer
	 */
	public function getCodigo()
	{
		return (double) $this->ped_idPedido;
	}

	public function getData()
	{
		return (string) $this->ped_data;
	}
	
	public function setCliente(App_Model_Entity_Cliente $value)
	{
		$this->objCliente = $value;
		$this->ped_idCliente = $value->getCodigo();
		return $this;
	}

	public function getCliente()
	{
		if (null == $this->objCliente && $this->getCodigo()) {
			$this->objCliente = $this->findParentRow(App_Model_DAO_Clientes::getInstance(), 'Cliente');
		}
		return $this->objCliente;
	}

	/**
	 * Recupera os items do pedido
	 * 
	 * @return App_Model_Entity_Pedido_Items
	 */
	public function getItems()
	{
		if (null == $this->objItems) {
			if ($this->getCodigo()) {
				$this->objItems = $this->findDependentRowset(App_Model_DAO_Pedidos_Items::getInstance(), 'Pedido');
				foreach ($this->objItems as $item) {
					$item->getProduto()->setPreco($item->ped_item_valorUnitario);
					$item->setPedido($this);
				}
				$this->objItems->rewind();
			} else {
				$this->objItems = App_Model_DAO_Pedidos_Items::getInstance()->createRowset()
					->on(App_Model_Collection::ON_AFTER_ADD, 'recalculaEntrega', $this)
					->on(App_Model_Collection::ON_AFTER_REMOVE, 'recalculaEntrega', $this);
			}
		}		
		$this->objItems->setPedido($this);		
		return $this->objItems;
	}

	/**
	 * Recupera a forma de entrega do pedido
	 *
	 * @return App_Model_Entity_Pedido_Entrega
	 */
	public function getEntrega()
	{
		if (null == $this->objEntrega) {
			if ($this->getCodigo()) {
				$this->objEntrega = App_Model_DAO_Pedidos_Entrega::getInstance()->fetchRow(
					App_Model_DAO_Pedidos_Entrega::getInstance()->select()->where('ped_ent_idPedido = ?', $this->getCodigo())
				);
			} else {
				$this->objEntrega = App_Model_DAO_Pedidos_Entrega::getInstance()->createRow();
			}
			$this->objEntrega->setPedido($this);
		}
		return $this->objEntrega;
	}

	/**
	 * Recupera o valor do pedido incluindo todos os itens e taxas
	 *
	 */
	public function getValor()
	{
		$valor = $this->getItems()->getValor();
		$valor += (float) ($this->getEntrega()->getServico() ? $this->getEntrega()->getServico()->getValor() : 0);
		return $valor;
	}

	/**
	 * Recupera o status atual do pedido
	 * 
	 * @return App_Model_Entity_Pedido_Status OR NULL
	 */
	public function getStatus()
	{
		return ($this->getHistoricoStatus()->count() ? $this->getHistoricoStatus()->seek($this->getHistoricoStatus()->count()-1)->current() : null);
	}

	/**
	 * Recupera o histórico de status do pedido
	 * 
	 * @return App_Model_Collection of App_Model_Entity_Pedido_Status
	 */
	public function getHistoricoStatus()
	{
		if (null == $this->objStatus) {
			if ($this->getCodigo()) {
				$this->objStatus = $this->findDependentRowset(
					App_Model_DAO_Pedidos_Status::getInstance(),
					'Pedido'
				);
			} else {
				$this->objStatus = App_Model_DAO_Pedidos_Status::getInstance()->createRowset();
			}
		}
		return $this->objStatus;
	}


	public function recalculaEntrega()
	{
		if (null !== $this->getEntrega()->getServico()){
			$this->getEntrega()->setServico(null);
		}
	}
}