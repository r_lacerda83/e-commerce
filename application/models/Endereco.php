<?php

class App_Model_Endereco extends App_Model_Listener
{
	const EVENT_CHANGE = 'setValue';
	const EVENT_CAPTURE = 'getValue';

	const CEP = 'cep';
	const LOGRADOURO = 'logradouro';
	const NUMERO = 'numero';
	const COMPLEMENTO = 'complemento';
	const BAIRRO = 'bairro';
	const CIDADE = 'cidade';
	const UF = 'uf';

	protected $parent;
	protected $fields;

	public function __construct(&$parent, $fields)
	{
		$this->parent = $parent;
		$this->fields = $fields;
	}

	public function __sleep()
	{
		return array_merge(parent::__sleep(), array('parent', 'fields'));
	}

	protected function setValue($field, $value)
	{
		$val = $this->fireEvent(self::EVENT_CHANGE);
		if ($val) $value = $val;

		$this->parent->{$this->fields[$field]} = $value;
	}

	protected function getValue($field)
	{
		$val = $this->fireEvent(self::EVENT_CAPTURE, array($field));
		return $val ? $val : $this->parent->{$this->fields[$field]};
	}

	/**
	 * Define o CEP do endere�o
	 * 
	 * @param string $value
	 * @return App_Model_Endereco
	 */
	public function setCep($value)
	{
		$this->setValue(self::CEP, $value);
		return $this;
	}

	/**
	 * Recupera o CEP do endere�o
	 * 
	 * @return string
	 */
	public function getCep()
	{
		return $this->getValue(self::CEP);
	}

	/**
	 * Define o logradouro do endere�o
	 * 
	 * @param string $value
	 * @return App_Model_Endereco
	 */
	public function setLogradouro($value)
	{
		$this->setValue(self::LOGRADOURO, $value);
		return $this;
	}

	/**
	 * Recupera o logradouro do endere�o
	 * 
	 * @return string
	 */
	public function getLogradouro()
	{
		return $this->getValue(self::LOGRADOURO);
	}

	/**
	 * Define o n�mero do endere�o
	 * 
	 * @param string $value
	 * @return App_Model_Endereco
	 */
	public function setNumero($value)
	{
		$this->setValue(self::NUMERO, $value);
		return $this;
	}

	/**
	 * Recupera o n�mero do endere�o
	 * 
	 * @return string
	 */
	public function getNumero()
	{
		return $this->getValue(self::NUMERO);
	}

	/**
	 * Define o complemento do endere�o
	 * 
	 * @param string $value
	 * @return App_Model_Endereco
	 */
	public function setComplemento($value)
	{
		$this->setValue(self::COMPLEMENTO, $value);
		return $this;
	}

	/**
	 * Recupera o complemento do endere�o
	 * 
	 * @return string
	 */
	public function getComplemento()
	{
		return $this->getValue(self::COMPLEMENTO);
	}

	/**
	 * Define o bairro do endere�o
	 * 
	 * @param string $value
	 * @return App_Model_Endereco
	 */
	public function setBairro($value)
	{
		$this->setValue(self::BAIRRO, $value);
		return $this;
	}

	/**
	 * Recupera o bairro do endere�o
	 * 
	 * @return string
	 */
	public function getBairro()
	{
		return $this->getValue(self::BAIRRO);
	}

	/**
	 * Define a cidade do endere�o
	 * 
	 * @param string $value
	 * @return App_Model_Endereco
	 */
	public function setCidade($value)
	{
		$this->setValue(self::CIDADE, $value);
		return $this;
	}

	/**
	 * Recupera a cidade do endere�o
	 * 
	 * @return string
	 */
	public function getCidade()
	{
		return $this->getValue(self::CIDADE);
	}

	/**
	 * Define a UF do endere�o
	 * 
	 * @param string $value
	 * @return App_Model_Endereco
	 */
	public function setUf($value)
	{
		$this->setValue(self::UF, $value);
		return $this;
	}

	/**
	 * Recupera a UF do endere�o
	 * 
	 * @return string
	 */
	public function getUf()
	{
		return $this->getValue(self::UF);
	}
}