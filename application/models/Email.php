<?php

class App_Model_Email
{
	/*
	 *  @var Zend_Mail_Transport
	 */
	protected $mailTransport = null;
	
	public function __construct()
	{
		$this->mailTransport = new Zend_Mail_Transport_Sendmail();
	}
	
	public function getFormaEnvio() {
		return $this->mailTransport;
	}
}