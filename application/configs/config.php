<?php
$BASE_HREF = 'http://192.168.0.7/e-commerce';

return array(
	'project' => 'E-commerce',
	'memCache' => array(
		'ativo' => false,
		'options' => array(
			'host' => 'localhost',
			'port' => 11211
		)
	),
	'gearman' => array(
		'host' => 'localhost',
	),
	'gremlin' => array(
		'enabled' => true,
		'host' => 'localhost',
		'port' => '8182'
	),
	'solr' => array(
		'ativo' => false,
		'options' => array(
			'hostname' => 'localhost',
			'path' => 'solr/busca',
			'port'     => 8983,
			'wt' => 'json'
		)
	),
	'paths' => array(
		'admin' => array(
			'root' => APPLICATION_ROOT .'/modules/admin/views',
			'file' => APPLICATION .'/public/admin',
			'base' => $BASE_HREF. '/public/admin'
		),
		'site' => array(
			'root' => APPLICATION_ROOT .'/modules/site/views',
			'file' => APPLICATION .'/public/site',
			'base' => $BASE_HREF. '/public/site'
		)
	)
);