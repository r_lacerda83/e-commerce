<?php

class Zend_View_Helper_TreeCategorias extends Zend_View_Helper_Abstract
{
	public function TreeCategorias()
	{
		//categorias
		$daoCategorias = new App_Model_DAO_Categorias();
		$rsCategorias = $daoCategorias->fetchAll(
			$daoCategorias->select()->where('ISNULL(cat_idCategoriaPai)')->order('cat_ordem ASC')
		);
		$categorias = $this->menuCategorias($rsCategorias);
		
		unset($rsCategorias, $daoCategorias);
		App_Funcoes_UTF8::encode($categorias);
		return Zend_Json::encode($categorias);
	}
	
	public function menuCategorias(App_Model_Collection $items)
	{
		$retorno = array();
		foreach ($items as $item) {
			$retorno[] = array(
				'id' => $item->getCodigo(),
				'text' => $item->getNome(),
				'cls' => ($item->getStatus() == false ? 'tree-status-inativo' : ''),
				'iconCls' => ($item->getChilds()->count() ? 'icon-folder-pai' : 'icon-folder'),
				'leaf' => ($item->getChilds()->count() ? false : true),
				'status' => $item->getStatus(),
				'checked' => false,
				'children' => $this->menuCategorias($item->getChilds())
			);
		}
		return $retorno;
	}
}
