<?php

class Zend_View_Helper_HasGroupPermission extends Zend_View_Helper_Abstract
{
	public function HasGroupPermission()
	{
		return $this;
	}
	
	public function verifica($group)
	{
		$usuario = App_Plugin_Login::getInstance()->getIdentity();
		$found = false;
		foreach ($usuario->getPerfil()->getPermissoes() as $acao) {
			if ($acao->getModulo()->getTab() == $group) {
				$found = true;
				break;
			}
		}
		return $found;
	}
	
	public function getActiveTab()
	{
		$usuario = App_Plugin_Login::getInstance()->getIdentity();
		$activeTab = 0;
		foreach (App_Funcoes_Rotulos::$tabs as $key => $tab) {
			foreach ($usuario->getPerfil()->getPermissoes() as $acao) {
				if ($acao->getModulo()->getTab() == $tab[0]) {
					return $key;					
				}
			}
		}
		return $activeTab;
	}
}