<?php

class ClientesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoClientes = App_Model_DAO_Clientes::getInstance();
				$filter = $daoClientes->select()
					->from($daoClientes)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'cli_nome')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('clientes' => array(), 'total' => 0);
					$rsClientes = $daoClientes->fetchAll($filter);
					foreach ($rsClientes as $cliente) {
						$retorno['clientes'][] = array(
							'cli_idCliente' => $cliente->getCodigo(),
							'cli_dataCadastro' => $cliente->getDataCadastro(),
							'cli_nascimento' => $cliente->getDataNascimento(),
							'cli_cpf' => $cliente->getCpf(),
							'cli_nome' => $cliente->getNome(),
							'cli_email' => $cliente->getEmail(),
							'cli_endereco_cidade' => $cliente->getEndereco()->getCidade(),
							'cli_endereco_uf' => $cliente->getEndereco()->getUF(),
							'cli_status' => (string) (int) $cliente->getStatus()
						);
					}
					$retorno['total'] = $daoClientes->getCount($filter);
					unset($rsClientes);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoClientes, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'cliente' => array());
			$daoClientes = App_Model_DAO_Clientes::getInstance();
			try {
				$cliente = $daoClientes->createRow();
				$cliente = $this->montaObjeto($cliente);

				try {
					$cliente->save();
					$retorno['success'] = true;
					$retorno['message'] = sprintf('Cliente <b>%s</b> cadastrado com sucesso.', $cliente->getNome());
				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					foreach ($retorno['errors'] as $erro) {
						if($erro['id'] == 'cli_cpfcnpj') {
							$retorno['errors'][] = array(
									'id' => $cliente->getTipoPessoa() == 'F' ? 'cli_cpf' : 'cli_cnpj',
									'msg' => $erro['msg']
							);
						}
					}
					throw new Exception('Por favor, verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o cliente.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoClientes, $cliente);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('cli_idCliente', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'cliente' => array());
			$daoClientes = App_Model_DAO_Clientes::getInstance();
			try {
				$cliente = $daoClientes->fetchRow(
					$daoClientes->select()->where('cli_idCliente = ?', $idRegistro)
				);
				if (null == $cliente) {
					throw new Exception('O cliente solicitado n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					//carrega os dados
					$retorno['success'] = true;
					$retorno['cliente'] = array($cliente->toArray());

				} else {
					//atualiza os dados
					$cliente = $this->montaObjeto($cliente);
					
					try {
						$cliente->save();
						$retorno['success'] = true;
						$retorno['message'] = sprintf('Cliente <b>%s</b> alterado com sucesso.', $cliente->getNome());

					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						foreach ($retorno['errors'] as $erro) {
							if($erro['id'] == 'cli_cpfcnpj') {
								$retorno['errors'][] = array(
									'id' => $cliente->getTipoPessoa() == 'F' ? 'cli_cpf' : 'cli_cnpj',
									'msg' => $erro['msg']
								);
							}
						}
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o cliente.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoClientes, $cliente);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}
	
	private function montaObjeto(App_Model_Entity_Cliente $cliente) {
		$daoClientes = App_Model_DAO_Clientes::getInstance();
		
		$cliente->setNome(utf8_decode($this->getRequest()->getParam('cli_nome')))
			->setCpf($this->getRequest()->getParam('cli_cpf'))
			->setRg($this->getRequest()->getParam('cli_rg'))
			->setDataNascimento(App_Funcoes_Date::conversion($this->getRequest()->getParam('cli_nascimento')))
			->setSexo($this->getRequest()->getParam('cli_sexo'))
			->setEmail(utf8_decode($this->getRequest()->getParam('cli_email')))
			->setSenha(utf8_decode($this->getRequest()->getParam('cli_senha')))
			->setTelefone($this->getRequest()->getParam('cli_telefone'))
			->setFax($this->getRequest()->getParam('cli_fax'))
			->setCelular($this->getRequest()->getParam('cli_celular'))
			->setFax($this->getRequest()->getParam('cli_fax'))
			->setNewsletter($this->getRequest()->getParam('cli_newsletter'))
			->setReferencia($this->getRequest()->getParam('cli_referencia'))
			->setTipoEndereco($this->getRequest()->getParam('cli_endereco_tipoEndereco'))
			->setStatus($this->getRequest()->getParam('cli_status'))
			->getEndereco()
				->setCEP($this->getRequest()->getParam('cli_endereco_cep'))
				->setLogradouro(utf8_decode($this->getRequest()->getParam('cli_endereco_endereco')))
				->setNumero($this->getRequest()->getParam('cli_endereco_numero'))
				->setComplemento(utf8_decode($this->getRequest()->getParam('cli_endereco_complemento')))
				->setBairro(utf8_decode($this->getRequest()->getParam('cli_endereco_bairro')))
				->setCidade(utf8_decode($this->getRequest()->getParam('cli_endereco_cidade')))
				->setUF(utf8_decode($this->getRequest()->getParam('cli_endereco_uf')));
				
		return $cliente;
	}
	
	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoClientes = App_Model_DAO_Clientes::getInstance();
		try {
			$cliente = $daoClientes->fetchRow(
				$daoClientes->select()->where('cli_idCliente = ?', $this->getRequest()->getParam('idCliente'))
			);
			if (null == $cliente) {
				throw new Exception('O cliente solicitado n�o foi encontrado.');
			}
			try {
				$nome = $cliente->getNome();
				$cliente->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Cliente <b>%s</b> removido com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o cliente.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoClientes, $cliente);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}
	
	public function enderecoAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => true, 'message' => null, 'errors' => array(), 'endereco' => array());
			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('endereco');
		}
	}
	
}