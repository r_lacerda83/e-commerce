<?php

class PedidosController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoPedidos = App_Model_DAO_Pedidos::getInstance();
				$daoPedidosStatus = App_Model_DAO_Pedidos_Status::getInstance();
				$daoClientes = App_Model_DAO_Clientes::getInstance();

				$selectStatus = $daoPedidosStatus->select()->from($daoPedidosStatus, 'MAX(ped_status_idStatus)')->where('ped_status_idPedido = ped_idPedido');

				$filter = $daoPedidos->getAdapter()->select()
					->from($daoPedidos->info('name'))
						->joinInner($daoPedidosStatus->info('name'), "ped_status_idPedido = ped_idPedido AND ped_status_idStatus = ({$selectStatus})", array('ped_status_valor'))
						->joinInner($daoClientes->info('name'), 'cli_idCliente = ped_idCliente',  array('cli_nome', 'cli_cpf'))
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'ped_data')} {$this->getRequest()->getParam('dir', 'DESC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('pedidos' => array(), 'total' => 0);
					$rsPedidos = $daoPedidos->createRowset($daoPedidos->getAdapter()->fetchAll($filter));

					foreach ($rsPedidos as $pedido) {
						$retorno['pedidos'][] = array(
							'ped_idPedido' => $pedido->getCodigo(),
							'ped_data' => $pedido->getData(),
							'cli_nome' => $pedido->getCliente()->getNome(),
							'cli_cpf' => $pedido->getCliente()->getCpf(),
							'ped_valor' => $pedido->getValor(),
							'ped_status_valor' => (string) $pedido['ped_status_valor']
						);
					}
					$filterCount = clone $filter;
					$filterCount->reset(Zend_Db_Select::COLUMNS)
						->columns('COUNT(1)')
						->reset(Zend_Db_Select::LIMIT_COUNT)
						->reset(Zend_Db_Select::LIMIT_OFFSET);
					$retorno['total'] = $daoPedidos->getAdapter()->fetchOne($filterCount);
					unset($rsPedidos);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoPedidos, $daoPedidosStatus, $daoClientes, $filter);
			break;
			
			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);

		}
	}

	public function detalheAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array();
			$daoPedidos = App_Model_DAO_Pedidos::getInstance();
			$daoPedidos->getAdapter()->query('SET SESSION wait_timeout = 100');

			$view = new Zend_View();
			$view->setBasePath(APPLICATION_PATH .DIRECTORY_SEPARATOR. 'views');

			try {
				$pedido = $daoPedidos->fetchRow(
					$daoPedidos->select()->where('ped_idPedido = ?', $this->getRequest()->getParam('idPedido'))
				);
				
				$view->pedido = $pedido;
				$retorno = array('conteudo' => $view->render('pedidos/detalhes-pedido.phtml'));
			} catch (Exception $e) {
				$retorno['message'] = $e->getMessage();
			}

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getFrontController()->setParam('noViewRenderer', false);
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		}
	}

	public function updatestatusAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => '', 'errors' => array());
			$daoPedidos = App_Model_DAO_Pedidos::getInstance();
			$pedido = $daoPedidos->fetchRow(
				$daoPedidos->select()->where('ped_idPedido = ?', $this->getRequest()->getParam('ped_idPedido'))
			);

			$status = App_Model_DAO_Pedidos_Status::getInstance()->createRow()
				->setValor($this->getRequest()->getParam('ped_status_status'))
				->setData(date('Y-m-d H:i:s'))
				->setDescricao(utf8_decode($this->getRequest()->getParam('ped_status_descricao')));

			$pedido->getHistoricoStatus()->offsetAdd($status);
			try {
				$pedido->save();

				$retorno['success'] = true;
				$retorno['message'] = 'Status do pedido atualizado com sucesso.';
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoPedidos, $pedido);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getFrontController()->setParam('noViewRenderer', false);
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		}
	}
}