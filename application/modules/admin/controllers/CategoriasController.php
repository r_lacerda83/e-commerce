<?php

class CategoriasController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	protected function montaFilhos(App_Model_Collection $items)
	{
		$retorno = array();
		foreach ($items as $node) {
			$retorno[] = array(
				'id' => $node->getCodigo(),
				'text' => $node->getNome(),
				'novo' => false,
				'cls' => ($node->getStatus() == false ? 'tree-status-inativo' : ''),
				'iconCls' => ($node->getChilds()->count() ? 'icon-folder-pai' : 'icon-folder'),
				'leaf' => ($node->getChilds()->count() ? false : true),
				'status' => (string) (int) $node->getStatus(),
				'ordem' => $node->getOrdem(),
				'children' => $this->montaFilhos($node->getChilds())
			);
		}
		return $retorno;
	}

	public function indexAction()
	{
		$daoCategorias = new App_Model_DAO_Categorias();
		$filter = $daoCategorias->select();

		switch ($this->getRequest()->getParam('load')) {
			case 'tree':
				$retorno = array();
				$categorias = $daoCategorias->fetchAll(
					$daoCategorias->select()->where('ISNULL(cat_idCategoriaPai)')
					->order('cat_nome ASC')
				);
				$retorno = $this->montaFilhos($categorias);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;

			case 'status':
				$retorno = array('errors'=>array());
				if(($idCategoria = $this->getRequest()->getParam('cat_idCategoria', false)) !== false) {
					try {
						$filter->add('cat_idCategoria = ?', $idCategoria);
						$categoria = $categorias->getCategoria($filter);

						$categoria->setStatus($this->getRequest()->getParam('cat_status'));

						$categorias->update($categoria);

						$retorno['success'] = true;
						$retorno['message'] = "Status da categoria <b>''{$categoria->getNome()}''</b> atualizado com sucesso.";

					}catch (Exception $e){
						$retorno['success'] = false;
						$retorno['message'] = $e->getMessage();
					}
				}
				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;
			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}

		unset($daoCategorias);
	}

	public function insertAction()
	{
		if($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'categoria' => array());
			try {
				$daoCategorias = App_Model_DAO_Categorias::getInstance();
				$categoria = $daoCategorias->createRow();
				$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();

				$categoria->setNome(utf8_decode($this->getRequest()->getParam('cat_nome')))
					->setPai(
						$daoCategorias->fetchRow(
							$daoCategorias->select()->where('cat_idCategoria = ?', $this->getRequest()->getParam('cat_idCategoriaPai'))
						)
					)
					->setOrdem($this->getRequest()->getParam('cat_ordem', 1))
					->setStatus($this->getRequest()->getParam('cat_status', 1));
					
				try {
					$categoria->save();

					$retorno['success'] = true;
					$retorno['message'] = sprintf('Categoria <b>%s</b> cadastrada com sucesso.', $categoria->getNome());
				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar a categoria.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('cat_idCategoria', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'categoria' => array());
			$daoCategorias = App_Model_DAO_Categorias::getInstance();
			try {
				$categoria = $daoCategorias->fetchRow(
					$daoCategorias->select()->where('cat_idCategoria = ?', $this->getRequest()->getParam('cat_idCategoria'))
				);
				if (null == $categoria) {
					throw new Exception('A categoria solicitada n�o foi encontrado.');
				}

				if (false != ($direction = $this->getRequest()->getParam('move', false))) {
					try {
						$operador = ('up' == $direction ? '<' : '>');
						$categoriaReplace = $daoCategorias->fetchRow(
							$daoCategorias->select()
								->where(null == $categoria->getPai() ? 'ISNULL(cat_idCategoriaPai)' : "cat_idCategoriaPai = {$categoria->getPai()->getCodigo()}")
								->where("cat_ordem {$operador} ?", $categoria->getOrdem())
								->order('cat_ordem '. ('up' == $direction ? 'DESC' : 'ASC'))
								->limit(1)
						);
						
						$categoriaReplace->setOrdem($categoria->getOrdem())->save();
						$categoria->setOrdem(('up' == $direction ? $categoria->getOrdem()-1 : $categoria->getOrdem()+1))->save();

						$retorno['success'] = true;
						$retorno['message'] = "Categoria movida com sucesso.";
					} catch (Exception $e) {
						throw new Exception($e->getMessage());
					}
				} else {
					if ($this->getRequest()->getParam('load')) {
						
						//carrega os dados
						$retorno['success'] = true;
						$arrCategoria = $categoria->toArray();
						$retorno['categoria'] = array($arrCategoria);
						
					} else {
						$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
						//atualiza os dados
						$categoria->setNome(utf8_decode($this->getRequest()->getParam('cat_nome')))
							->setPai(
								$daoCategorias->fetchRow(
									$daoCategorias->select()->where('cat_idCategoria = ?', $this->getRequest()->getParam('cat_idCategoriaPai'))
								)
							)
							->setOrdem($this->getRequest()->getParam('cat_ordem', 1))
							->setStatus($this->getRequest()->getParam('cat_status', 1));
					
						try {
							$categoria->save();

							$retorno['success'] = true;
							$retorno['message'] = sprintf('Categoria <b>%s</b> alterada com sucesso.', $categoria->getNome());
						} catch (App_Validate_Exception $e) {
							$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
							throw new Exception('Por favor verifique os campos marcados em vermelho.');
						} catch (Exception $e) {
							throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar a categoria.');
						}
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoCategorias, $categoria);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}

	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => '');
		$daoCategorias = App_Model_DAO_Categorias::getInstance();
		try {
			$categoria = $daoCategorias->fetchRow(
				$daoCategorias->select()->where('cat_idCategoria = ?', $this->getRequest()->getParam('cat_idCategoria'))
			);
			if (null == $categoria) {
				throw new Exception('Categoria n�o encontrada');
			}
			try {
				$ordem = $categoria->getOrdem();
				$nome = $categoria->getNome();
				$pai = $categoria->getPai();
				$categoria->delete();
				
				// Atualizar ordem das categorias
				$proximasCategorias = $daoCategorias->fetchAll(
					$daoCategorias->select()
						->where(null == $pai ? 'ISNULL(cat_idCategoriaPai)' : "cat_idCategoriaPai = {$pai->getCodigo()}")
						->where('cat_ordem > ?', $ordem)
				);
				foreach ($proximasCategorias as $replaceCategoria) {
					$replaceCategoria->setOrdem($ordem);
					$replaceCategoria->save();
					$ordem++;
				}

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Categoria <b>%s</b> removida com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a categoria');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoCategorias, $categoria);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}
}