<?php

class ProdutosController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$daoProdutosSKU = App_Model_DAO_Produtos_SKU::getInstance();
				$daoMarcas = App_Model_DAO_Marcas::getInstance();

				$filter = $daoProdutos->getAdapter()->select()
					->from($daoProdutos->info('name'))
						->joinInner($daoMarcas->info('name'), 'marc_idMarca = prod_idMarca',  null)
						->joinInner($daoProdutosSKU->info('name'), 'prod_sku_idProduto = prod_idProduto',  null)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'prod_nome')} {$this->getRequest()->getParam('dir', 'ASC')}")
					->group('prod_idProduto');

				if(null != ($marca = $this->getRequest()->getParam('marca', null))) {
					$filter->where('prod_idMarca = ?', $marca);
				}

				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('produtos' => array(), 'total' => 0);
					$rsProdutos = $daoProdutos->createRowset(
						$daoProdutos->getAdapter()->fetchAll($filter, null, Zend_Db::FETCH_ASSOC)
					);

					foreach ($rsProdutos as $produto) {
						$item = array(
							'prod_idProduto' => $produto->getCodigo(),
							'prod_hasSKU' => false,
							'prod_sku_SKU' => $produto->getCodigo(),
							'prod_nome' => $produto->getNome(),
							'marc_nome' => $produto->getMarca()->getNome(),
							'prod_sku_estoque' => $produto->getEstoque(),
							'prod_status' => (int) $produto->getStatus(),
							'prod_variacoes' => array()
						);
						foreach ($produto->getSKU() as $sku) {
							$item['prod_variacoes'][] = array(
								'prod_hasSKU' => true,
								'prod_sku_SKU' => $sku->getSKU(),
								'prod_nome' => $sku->getNome(false),
								'prod_sku_preco' => $sku->getPrecoOriginal(),
								'prod_sku_estoque' => $sku->getEstoque(),
								'prod_status' => (int) $sku->getStatus()
							);
						}
						$retorno['produtos'][] = $item;
					}
					$filterCount = clone $filter;
					$filterCount->reset(Zend_Db_Select::LIMIT_COUNT)->reset(Zend_Db_Select::LIMIT_OFFSET);
					$filterCount = $daoProdutos->getAdapter()->select()->from($filterCount, 'COUNT(1)');

					$retorno['total'] = $daoProdutos->getAdapter()->fetchOne($filterCount);
					unset($rsProdutos);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoProdutos, $daoProdutosSKU, $daoMarcas, $filter);
				break;

			case 'sku':
				$retorno = array('exists' => false);
				if (false != ($sku = $this->getRequest()->getParam('sku', false))) {
					$daoProdutosSKU = App_Model_DAO_Produtos_SKU::getInstance();
					try {
						$retorno['exists'] = ($daoProdutosSKU->getCount($daoProdutosSKU->select()->from($daoProdutosSKU)->where('prod_sku_SKU = ?', $sku)) > 0);
					} catch (Exception $e) {
						$retorno['exists'] = false;
					}
					unset($daoProdutosSKU);
				}
				echo Zend_Json::encode($retorno);
			break;
				
			case 'limpa-cache':
				$retorno = array('success' => true, 'message' => 'Cache limpo com sucesso');
				$config = Zend_Registry::get('config');
				
				if($config->memCache->ativo) {
					$cache = Zend_Registry::get('memCache');
					$cache->clean(Zend_Cache::CLEANING_MODE_ALL);
				} else {
					$retorno = array('success' => false, 'message' => 'Memcache nao esta ativo');
				}
				
				unset($cache, $config);
				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;
			
			case 'indexar':
				set_time_limit(0);
				$retorno = array('success' => true, 'message' => 'Indexa��o finalizada com sucesso');
				$config = Zend_Registry::get('config');
				
				//Limpando o cache atual
				$url = "http://{$config->solr->options->hostname}:{$config->solr->options->port}/{$config->solr->options->path}/update?stream.body=<delete><query>*:*</query></delete>&commit=true";
				$fopen = fopen($url, 'r');
				
				if($config->solr->ativo) {
					$daoProdutos = App_Model_DAO_Produtos::getInstance();
					$daoProdutosSKU = App_Model_DAO_Produtos_SKU::getInstance();
					$daoMarcas = App_Model_DAO_Marcas::getInstance();
					
					$filter = $daoProdutos->getAdapter()->select()
						->from($daoProdutos->info('name'))
						->joinInner($daoMarcas->info('name'), 'marc_idMarca = prod_idMarca',  null)
						->joinInner($daoProdutosSKU->info('name'), 'prod_sku_idProduto = prod_idProduto')
						->group('prod_idProduto');
					
					$rsProdutos = $daoProdutos->createRowset(
						$daoProdutos->getAdapter()->fetchAll($filter, null, Zend_Db::FETCH_ASSOC)
					);
					
					//Indexando
					$config = Zend_Registry::get('config');
					$client = new SolrClient($config->solr->options->toArray());
					
					foreach ($rsProdutos as $produto) {
						$doc = new SolrInputDocument();
						foreach($produto->getCategorias() as $categoria) {
							$doc->addField('cat', $categoria->getCodigo());
						}
						
						$doc->addField('id', $produto->getCodigo());
						$doc->addField('sku', $produto->getVitrine()->getSKU());
						$doc->addField('nome', utf8_encode($produto->getNome()));
						$doc->addField('descricao', utf8_encode($produto->getDescricao()));
						$doc->addField('preco', $produto->getVitrine()->getPreco());
						$doc->addField('imagem', $produto->getVitrine()->getImagens()->count() ? $produto->getVitrine()->getImagens()->current()->getBasePath('vitrine') : 'images/home/product6.jpg');
						
						$updateResponse = $client->addDocument($doc);
						$client->commit();
						unset($doc);
					}
					unset($rsProdutos, $client, $daoProdutos, $daoProdutosSKU, $daoMarcas, $config);
				} else {
					$retorno = array('success' => false, 'message' => 'Solr n�o est� ativo');
				}
				
				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			break;
				

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'produto' => array());
			$daoProdutos = App_Model_DAO_Produtos::getInstance();
			try {
				$produto = $daoProdutos->createRow();
				$produto = $this->montaObjeto($produto);				

				try {
					$produto->save();

					$retorno['success'] = true;
					$retorno['message'] = sprintf('Produto <b>%s</b> cadastrado com sucesso.', $produto->getNome());
				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o produto.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoProdutos, $daoFornecedores, $produto);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->buildForm();
		}
	}

	public function updateAction()
	{
		if (false != ($prod_sku_SKU = $this->getRequest()->getParam('removeSKU', false))) {
			$retorno = array('success' => false, 'message' => '');
			$daoSKU = App_Model_DAO_Produtos_SKU::getInstance();
			try {
				$sku = $daoSKU->fetchRow($daoSKU->select()->where('prod_sku_SKU = ?', $prod_sku_SKU));
				if (null == $sku) {
					throw new Exception('O SKU solicitado n�o foi encontrado.');
				}

				try {
					$sku->delete();

					$retorno['success'] = true;
					$retorno['message'] = 'SKU removido com sucesso.';
				} catch (Exception $e) {
					throw new Exception('N�o foi poss�vel remover o SKU<br />Possivelmente j� houve uma venda deste sku ou ele est� vinculado a algum outro cadastro.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoSKU);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			if (false != ($idRegistro = $this->getRequest()->getParam('prod_idProduto', false))) {
				$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'produto' => array());
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
				try {
					$produto = $daoProdutos->fetchRow(
						$daoProdutos->select()->where('prod_idProduto = ?', $idRegistro)
					);
					if (null == $produto) {
						throw new Exception('O produto solicitado n�o foi encontrado.');
					}

					if ($this->getRequest()->getParam('load')) {
						//carrega os dados
						$retorno['success'] = true;
						$arrProduto = $produto->toArray();

						foreach ($produto->getCategorias() as $categoria) {
							$arrProduto['prod_categorias'][] = $categoria->getCodigo();
						}

						foreach ($produto->getVariacoes() as $variacao) {
							$arrProduto['prod_variacoes'][] = array(
								'prod_var_idVariacao' => $variacao->getCodigo(),
								'prod_var_nome' => $variacao->getNome(),
								'prod_var_ordem' => $produto->getVariacoes()->key() + 1
							);
						}

						foreach ($produto->getSKU() as $sku) {
							$arrSKU = $sku->toArray();
							$arrSKU['prod_sku_new'] = false;
							$arrSKU['prod_sku_preco'] = App_Funcoes_Money::toCurrency($sku->getPrecoOriginal());
							$arrSKU['prod_sku_custo'] = App_Funcoes_Money::toCurrency($arrSKU['prod_sku_custo']);
							
							$arrSKU['prod_sku_peso'] = number_format($arrSKU['prod_sku_peso'], 3, '.', '.');
							$arrSKU['prod_sku_altura'] = number_format($arrSKU['prod_sku_altura'], 2, '.', '.');
							$arrSKU['prod_sku_largura'] = number_format($arrSKU['prod_sku_largura'], 2, '.', '.');
							$arrSKU['prod_sku_comprimento'] = number_format($arrSKU['prod_sku_comprimento'], 2, '.', '.');
							$arrSKU['prod_sku_presentePreco'] = App_Funcoes_Money::toCurrency($arrSKU['prod_sku_presentePreco']);
							$arrSKU['prod_sku_ordem'] = (int) $arrSKU['prod_sku_ordem'];

							foreach ($sku->getAtributos() as $atributo) {
								$arrSKU["var_label_{$atributo->getVariacao()->getCodigo()}"] = "[{$atributo->getCodigo()}] - {$atributo->getValor()}";
							}

							$arrSKU['prod_sku_imagens'] = array();
							foreach ($sku->getImagens() as $imagem) {
								$arrSKU['prod_sku_imagens'][] = array(
									'gal_arq_idArquivo' => $imagem->getCodigo(),
									'gal_arq_nome' => substr($imagem->getNome(), 0, strrpos($imagem->getNome(), '.')),
									'gal_arq_ordem' => $sku->getImagens()->key()+1
								);
							}

							$arrProduto['prod_sku'][] = $arrSKU;
						}

						$retorno['produto'] = array($arrProduto);
					} else {
						$produto = $this->montaObjeto($produto);
						
						try {
							$produto->save();

							$retorno['success'] = true;
							$retorno['message'] = sprintf('Produto <b>%s</b> alterado com sucesso.', $produto->getNome());
						} catch (App_Validate_Exception $e) {
							$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
							throw new Exception('Por favor verifique os campos marcados em vermelho.');
						} catch (Exception $e) {
							throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o produto.');
						}
					}
				} catch (Exception $e) {
					$retorno['success'] = false;
					$retorno['message'] = $e->getMessage();
				}
				unset($daoProdutos, $produto);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
			} else {
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->buildForm();
			}
		}
	}
	
	private function montaObjeto(App_Model_Entity_Produto $produto){
		$daoMarcas = App_Model_DAO_Marcas::getInstance();
		$produto->setNome(utf8_decode($this->getRequest()->getParam('prod_nome')))
			->setMarca(
				$daoMarcas->fetchRow(
					$daoMarcas->select()->where('marc_idMarca = ?', $this->getRequest()->getParam('prod_idMarca'))
				)
			)
			->setTags(utf8_decode($this->getRequest()->getParam('prod_tags')))
			->setMetaTitle(utf8_decode($this->getRequest()->getParam('prod_meta_title')))
			->setMetaTags(utf8_decode($this->getRequest()->getParam('prod_meta_tags')))
			->setMetaDescription(utf8_decode($this->getRequest()->getParam('prod_meta_description')))
			->setDestaqueHome($this->getRequest()->getParam('prod_destaqueHome'))
			->setDescricao(utf8_decode($this->getRequest()->getParam('prod_descricao')))
			->setStatus($this->getRequest()->getParam('prod_status'));
			
		unset($daoMarcas);
		
		//categorias
		if ($this->getRequest()->getParam('prod_categorias')) {
			$daoCategorias = App_Model_DAO_Categorias::getInstance();
			$prod_categorias = Zend_Json::decode($this->getRequest()->getParam('prod_categorias', array()));
			$produto->getCategorias()->offsetRemoveAll();
			foreach ($prod_categorias as $idCategoria) {
				$produto->getCategorias()->offsetAdd(
						$daoCategorias->fetchRow(
								$daoCategorias->select()->where('cat_idCategoria = ?', $idCategoria)
						)
				);
			}
			unset($daoCategorias);
		}
		
		//varia��es
		$variacoes = Zend_Json::decode($this->getRequest()->getParam('prod_variacoes'));
		$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
		$produto->getVariacoes()->offsetRemoveAll();
		foreach ($variacoes as $variacao) {
			$produto->getVariacoes()->offsetAdd(
					$daoVariacoes->fetchRow(
							$daoVariacoes->select()->where('var_idVariacao = ?', $variacao['prod_var_idVariacao'])
					)
			);
		}
		unset($daoVariacoes);
		
		$variacoes = Zend_Json::decode($this->getRequest()->getParam('prod_sku'));
		if (is_array($variacoes)) {
			$daoAtributos = App_Model_DAO_Variacoes_Atributos::getInstance();
			foreach ($variacoes as $variacao) {
		
				if ($variacao['prod_sku_new'] == true) {
					$sku = $produto->getSKU()->createRow()
					->setSKU($variacao['prod_sku_SKU'] ? $variacao['prod_sku_SKU'] : App_Model_DAO_Produtos_SKU::getInstance()->generateSKU())
					->setEstoque($variacao['prod_sku_estoque']);
				} else {
					$sku = $produto->getSKU()->find('prod_sku_SKU', $variacao['prod_sku_SKU']);
				}
		
				$sku->setProduto($produto)
					->setVitrine($variacao['prod_sku_vitrine'])
					->setPreco(App_Funcoes_Money::toFloat($variacao['prod_sku_preco']))
					->setPeso($variacao['prod_sku_peso'])
					->setAltura($variacao['prod_sku_altura'])
					->setLargura($variacao['prod_sku_largura'])
					->setComprimento($variacao['prod_sku_comprimento'])
					->setCusto(App_Funcoes_Money::toFloat($variacao['prod_sku_custo']))
					->setEstoque($variacao['prod_sku_estoque'])
					->setEstoqueMinimo($variacao['prod_sku_estoqueMinimo'])
					->setPresente($variacao['prod_sku_presente'])
					->setPresentePreco(App_Funcoes_Money::toFloat($variacao['prod_sku_presentePreco']))
					->setOrdem($variacao['prod_sku_ordem'])
					->setStatus($variacao['prod_sku_status']);
		
				//imagens da varia��o
				$sku->getImagens()->offsetRemoveAll();
				$daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
				foreach ($variacao['prod_sku_imagens'] as $idImagem) {
					$sku->getImagens()->offsetAdd(
						$daoImagens->fetchRow(
							$daoImagens->select()->where('gal_arq_idArquivo = ?', $idImagem)
						)
					);
				}
		
				//atributos que comp�em o SKU
				$sku->getAtributos()->offsetRemoveAll();
				foreach ($variacao['prod_sku_atributos'] as $idAtributo) {
					$sku->getAtributos()->offsetAdd(
						$daoAtributos->fetchRow(
							$daoAtributos->select()->where('var_atr_idAtributo = ?', $idAtributo)
						)
					);
				}
		
				$produto->getSKU()->offsetAdd($sku);
			}
			unset($daoAtributos, $daoImagens);
		}
		
		return $produto;
	}

	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoProdutos = App_Model_DAO_Produtos::getInstance();
		try {
			$produto = $daoProdutos->fetchRow(
				$daoProdutos->select()->where('prod_idProduto = ?', $this->getRequest()->getParam('idProduto'))
			);
			if (null == $produto) {
				throw new Exception('O produto solicitado n�o foi encontrado.');
			}
			try {
				$nome = $produto->getNome();
				$produto->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Produto <b>%s</b> removido com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o produto.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoProdutos, $produto);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}

	protected function buildForm()
	{
		$menuVariacoes = array();

		$daoCategorias = new App_Model_DAO_Categorias();
		$rsCategorias = $daoCategorias->fetchAll(
			$daoCategorias->select()->where('ISNULL(cat_idCategoriaPai)')
			->order('cat_nome ASC')
		);
		$categorias = $this->menuCategorias($rsCategorias);
		App_Funcoes_UTF8::encode($categorias);
		$this->view->categorias = Zend_Json::encode($categorias);
		unset($rsCategorias, $daoCategorias);

		$storeVariacoes = array('prod_sku_new', 'prod_sku_SKU', 'prod_sku_destaque', 'prod_sku_refFornecedor', 'prod_sku_ean', 'prod_sku_st', 'prod_sku_ncm', 'prod_sku_preco', 'prod_sku_custo', 'prod_sku_peso', 'prod_sku_estoque', 'prod_sku_estoqueMinimo', 'prod_sku_presente', 'prod_sku_presenteValor', 'prod_sku_idImagem1', 'prod_sku_idImagem2', 'prod_sku_idImagem3', array('name' => 'prod_sku_ordem', 'type' => 'int'), 'prod_sku_status');
		$columnsVariacoes = array(
			array(
				'header' => 'SKU',
				'dataIndex' => 'prod_sku_SKU',
				'hidden' => false,
				'width' => 130,
				'editor' => array(
					'xtype' => 'textfield',
					'maxLength' =>  20,
					'allowBlank' => true
				)
			)
		);
		$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
		$rsVariacoes = $daoVariacoes->fetchAll($daoVariacoes->select()->order('var_nome ASC'));
		$listaVariacoes = array();
		foreach ($rsVariacoes as $variacao) {
			$editorOptions = array();
			foreach ($variacao->getAtributos() as $atributo) {
				$editorOptions[] = array($atributo->getCodigo(), utf8_encode("[{$atributo->getCodigo()}] - {$atributo->getValor()}"));
			}
			$columnsVariacoes[] = array(
				'header' => utf8_encode($variacao->getNome()),
				'dataIndex' => "var_label_{$variacao->getCodigo()}",
				'id' => "var_label_{$variacao->getCodigo()}",
				'editor' => array(
					'xtype' => 'combo',
					'store' => array(
						'xtype' => 'simplestore',
						'fields' => array('id', 'value'),
						'data' => $editorOptions
					),
					'displayField' => 'value',
					'valueField' => 'value',
					'editable' =>  false,
					'tpl' => '<tpl for="."><div ext:qtip="{value}" class="x-combo-list-item">{value}</div></tpl>',
					'mode' => 'local',
					'triggerAction' => 'all'
				)
			);
			$listaVariacoes[] = array($variacao->getCodigo(), $variacao->getNome());
			$menuVariacoes[] = array(
				'text' => utf8_encode($variacao->getNome()),
				'var_idVariacao' => $variacao->getCodigo()
			);
		}
		unset($daoVariacoes, $rsVariacoes);
		$this->view->menuVariacoes = Zend_Json::encode($menuVariacoes);
		$this->view->variacoes = Zend_Json::encode($listaVariacoes);
		$this->view->storeVariacoes = Zend_Json::encode($storeVariacoes);
		$this->view->columnsVariacoes = Zend_Json::encode($columnsVariacoes);

		//marcas
		$daoMarcas = App_Model_DAO_Marcas::getInstance();
		$rsMarcas = $daoMarcas->fetchAll($daoMarcas->select()->order('marc_nome ASC'));
		$marcas = array();
		foreach ($rsMarcas as $marca) {
			$marcas[] = array(
				$marca->getCodigo(),
				$marca->getNome(),
				$marca->getStatus()
			);
		}
		unset($daoMarcas, $rsMarcas);
		App_Funcoes_UTF8::encode($marcas);
		$this->view->marcas = Zend_Json::encode($marcas);

		$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
		$this->render('form');
	}

	protected function menuCategorias(App_Model_Collection $items)
	{
		$retorno = array();
		foreach ($items as $item) {
			$retorno[] = array(
				'id' => $item->getCodigo(),
				'text' => $item->getNome(),
				'cls' => ($item->getStatus() == false ? 'tree-status-inativo' : ''),
				'iconCls' => ($item->getChilds()->count() ? 'icon-folder-pai' : 'icon-folder'),
				'leaf' => ($item->getChilds()->count() ? false : true),
				'status' => $item->getStatus(),
				'checked' => false,
				'children' => $this->menuCategorias($item->getChilds())
			);
		}
		return $retorno;
	}
}