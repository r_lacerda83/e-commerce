<?php

class VariacoesController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'tree':
				$retorno = array();
				$daoVariacoes = App_Model_DAO_Variacoes::getInstance();

				$filter = $daoVariacoes->select()->order('var_nome ASC');
				$rsVariacoes = $daoVariacoes->fetchAll($filter);
				foreach ($rsVariacoes as $variacao) {
					$item = array(
						'id' => str_pad($variacao->getCodigo(), 10, 0, STR_PAD_LEFT),
						'text' => "{$variacao->getNome()} ({$variacao->getRotulo()})",
						'cls' => 'folder',
						'tipo' => 'V',
						'iconCls' => ($variacao->getAtributos()->count() ? 'icon-folder-pai' : 'icon-folder'),
						'leaf' => ($variacao->getAtributos()->count() ? false : true)
					);
					foreach ($variacao->getAtributos() as $atributo) {
						$item['children'][] = array(
							'id' => $atributo->getCodigo(),
							'text' => "[{$atributo->getCodigo()}] - {$atributo->getValor()}",
							'status' => (string) (int) $atributo->getStatus(),
							'cls' => "file ".($atributo->getStatus() == false ? 'tree-status-inativo' : ''),
							'tipo' => 'A',
							'iconCls' => 'icon-file',
							'icon' => ($atributo->getImagem() != null ? $atributo->getImagem()->getBasePath() : ''),
							'idImagem' => ($atributo->getImagem() != null ? $atributo->getImagem()->getCodigo() : ''),
							'leaf' => true
						);
					}
					$retorno[] = $item;
				}
				unset($daoVariacoes, $rsVariacoes, $filter);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		switch ($this->getRequest()->getParam('tipo')) {
			case 'variacao':
				if ($this->getRequest()->isPost()) {
					$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'variacao' => array());
					$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
					$daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
					try {
						$variacao = $daoVariacoes->createRow();
						$variacao->setNome(utf8_decode($this->getRequest()->getParam('var_nome')))
							->setRotulo(utf8_decode($this->getRequest()->getParam('var_rotulo')));
						if (false != $this->getRequest()->getParam('var_idImagem', false)) {
							$variacao->setImagem(
								$daoImagens->fetchRow(
									$daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('var_idImagem'))
								)
							);
						}

						try {
							$variacao->save();

							$retorno['success'] = true;
							$retorno['message'] = sprintf('Varia��o <b>%s</b> cadastrada com sucesso.', $variacao->getNome());
						} catch (App_Validate_Exception $e) {
							$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
							throw new Exception('Por favor verifique os campos marcados em vermelho.');
						} catch (Exception $e) {
							throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar a varia��o.');
						}
					} catch (Exception $e) {
						$retorno['success'] = false;
						$retorno['message'] = $e->getMessage();
					}
					unset($daoVariacoes, $daoImagens, $variacao);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				} else {
					$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
					$this->render('form');
				}
			break;

			case 'atributo':
				if ($this->getRequest()->isPost()) {
					$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'atributo' => array());
					$daoAtributos = App_Model_DAO_Variacoes_Atributos::getInstance();
					$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
					$daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
					try {
						$atributo = $daoAtributos->createRow();
						$atributo->setValor(utf8_decode($this->getRequest()->getParam('var_atr_valor')))
							->setVariacao(
								$daoVariacoes->fetchRow(
									$daoVariacoes->select()->where('var_idVariacao = ?', $this->getRequest()->getParam('var_atr_idVariacao'))
								)
							)
							->setOrdem($atributo->getVariacao()->getAtributos()->count() + 1)
							->setStatus($this->getRequest()->getParam('var_atr_status'));
						if ($this->getRequest()->getParam('var_atr_idImagem', false) != false) {
							$atributo->setImagem(
								$daoImagens->fetchRow(
									$daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('var_atr_idImagem'))
								)
							);
						}

						try {
							$atributo->save();

							$retorno['success'] = true;
							$retorno['message'] = sprintf('Atributo da varia��o <b>%s</b> cadastrado com sucesso.', $atributo->getValor());
						} catch (App_Validate_Exception $e) {
							$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
							throw new Exception('Por favor verifique os campos marcados em vermelho.');
						} catch (Exception $e) {
							throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar o atributo da varia��o.');
						}
					} catch (Exception $e) {
						$retorno['success'] = false;
						$retorno['message'] = $e->getMessage();
					}
					unset($daoAtributos, $daoVariacoes, $daoImagens, $atributo);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				} else {
					$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
					$this->render('form-atributo');
				}
			break;
		}
	}

	public function updateAction()
	{
		switch ($this->getRequest()->getParam('tipo')) {
			case 'variacao':
				if (false != ($idRegistro = $this->getRequest()->getParam('var_idVariacao', false))) {
					$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'variacao' => array());
					$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
					$daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
					try {
						$variacao = $daoVariacoes->fetchRow(
							$daoVariacoes->select()->where('var_idVariacao = ?', $idRegistro)
						);
						if (null == $variacao) {
							throw new Exception('A varia��o solicitada n�o foi encontrado.');
						}

						if ($this->getRequest()->getParam('load')) {
							// carrega os dados
							$retorno['success'] = true;
							$retorno['variacao'] = array($variacao->toArray());
							$retorno['variacao'][0]['var_idImagem'] = (null != $variacao->getImagem() ? $variacao->getImagem()->getCodigo() : null);
						} else {
							// atualiza os dados
							$variacao->setNome(utf8_decode($this->getRequest()->getParam('var_nome')))
								->setRotulo(utf8_decode($this->getRequest()->getParam('var_rotulo')));
							if (false != $this->getRequest()->getParam('var_idImagem', false)) {
								$variacao->setImagem(
									$daoImagens->fetchRow(
										$daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('var_idImagem'))
									)
								);
							} else {
								$variacao->setImagem(null);
							}

							try {
								$variacao->save();

								$retorno['success'] = true;
								$retorno['message'] = sprintf('Varia��o <b>%s</b> alterada com sucesso.', $variacao->getNome());
							} catch (App_Validate_Exception $e) {
								$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
								throw new Exception('Por favor verifique os campos marcados em vermelho.');
							} catch (Exception $e) {
								throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar a varia��o.');
							}
						}
					} catch (Exception $e) {
						$retorno['success'] = false;
						$retorno['message'] = $e->getMessage();
					}
					unset($daoVariacoes, $daoImagens, $variacao);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				} else {
					$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
					$this->render('form');
				}
				break;

			case 'atributo':
				if (false != ($idRegistro = $this->getRequest()->getParam('var_atr_idAtributo', false))) {
					if (false != $this->getRequest()->getParam('move', false)) {
						$direction = $this->getRequest()->getParam('direction');
						$daoAtributos = App_Model_DAO_Variacoes_Atributos::getInstance();
						$atributo = $daoAtributos->fetchRow(
							$daoAtributos->select()->where('var_atr_idAtributo = ?', $this->getRequest()->getParam('var_atr_idAtributo'))
						);

						$replacedAtributo = $daoAtributos->fetchRow(
							$daoAtributos->select()->where('var_atr_idVariacao = ?', $atributo->getVariacao()->getCodigo())
								->where('var_atr_ordem = ?', ($direction == 'down' ? $atributo->getOrdem() + 1 : $atributo->getOrdem() - 1))
						);

						$replacedAtributo->setOrdem($atributo->getOrdem());
						$atributo->setOrdem(($direction == 'down' ? $atributo->getOrdem() + 1 : $atributo->getOrdem() - 1));

						$retorno = array('success' => false, 'message' => '');
						$daoAtributos->getAdapter()->beginTransaction();
						try {
							$atributo->save();
							$replacedAtributo->save();
							$daoAtributos->getAdapter()->commit();

							$retorno['success'] = true;
							$retorno['message'] = 'Ordena��o do atributo da varia��o alterada com sucesso.';
						} catch (Exception $e) {
							$daoAtributos->getAdapter()->rollBack();
							$retorno['success'] = false;
							$retorno['message'] = 'N�o foi poss�vel alterar a ordena��o do atributo da varia��o.';
						}
						unset($daoAtributos);

						App_Funcoes_UTF8::encode($retorno);
						echo Zend_Json::encode($retorno);
					} else {
						$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'atributo' => array());
						$daoAtributos = App_Model_DAO_Variacoes_Atributos::getInstance();
						$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
						$daoImagens = App_Model_DAO_Galerias_Arquivos::getInstance();
						try {
							$atributo = $daoAtributos->fetchRow(
								$daoAtributos->select()->where('var_atr_idAtributo = ?', $idRegistro)
							);
							if (null == $atributo) {
								throw new Exception('O atributo da varia��o solicitado n�o foi encontrado.');
							}

							if ($this->getRequest()->getParam('load')) {
								// carrega os dados
								$retorno['success'] = true;
								$retorno['atributo'] = array($atributo->toArray());
							} else {
								// atualiza os dados
								if($this->getRequest()->getParam('var_atr_valor', false) != false) {
									$atributo->setValor(utf8_decode($this->getRequest()->getParam('var_atr_valor')));
								}
								
								$atributo->setVariacao(
									$daoVariacoes->fetchRow(
										$daoVariacoes->select()->where('var_idVariacao = ?', $this->getRequest()->getParam('var_atr_idVariacao'))
									)
								)
								->setStatus($this->getRequest()->getParam('var_atr_status'));
								
								if ($this->getRequest()->getParam('var_atr_idImagem', false) != false) {
									$atributo->setImagem(
										$daoImagens->fetchRow(
											$daoImagens->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('var_atr_idImagem'))
										)
									);
								} else {
									$atributo->setImagem(null);
								}

								try {
									$atributo->save();

									$retorno['success'] = true;
									$retorno['message'] = sprintf('Atributo da varia��o <b>%s</b> alterado com sucesso.', $atributo->getVariacao()->getNome());
								} catch (App_Validate_Exception $e) {
									$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
									throw new Exception('Por favor verifique os campos marcados em vermelho.');
								} catch (Exception $e) {
									throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar o atributo da varia��o.');
								}
							}
						} catch (Exception $e) {
							$retorno['success'] = false;
							$retorno['message'] = $e->getMessage();
						}
						unset($daoAtributos, $daoVariacoes, $daoImagens, $atributo);

						App_Funcoes_UTF8::encode($retorno);
						echo Zend_Json::encode($retorno);
					}
				} else {
					$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
					$this->render('form-atributo');
				}
				break;
		}
	}

	public function deleteAction()
	{
		switch ($this->getRequest()->getParam('tipo')) {
			case 'variacao':
				$retorno = array('success' => false, 'message' => null);
				$daoVariacoes = App_Model_DAO_Variacoes::getInstance();
				try {
					$variacao = $daoVariacoes->fetchRow(
						$daoVariacoes->select()->where('var_idVariacao = ?', $this->getRequest()->getParam('var_idVariacao'))
					);
					if (null == $variacao) {
						throw new Exception('A varia��o solicitada n�o foi encontrada.');
					}

					try {
						$nome = $variacao->getNome();
						$variacao->delete();
						$retorno['success'] = true;
						$retorno['message'] = sprintf('Varia��o <b>%s</b> removida com sucesso.', $nome);

					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a varia��o.');
					}

				} catch (Exception $e) {
					$retorno['success'] = false;
					$retorno['message'] = $e->getMessage();
				}

				unset($daoVariacoes, $variacao);
				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				break;

			case 'atributo':
				$retorno = array('success' => false, 'message' => null);
				$daoAtributos = App_Model_DAO_Variacoes_Atributos::getInstance();
				try {
					$atributo = $daoAtributos->fetchRow(
						$daoAtributos->select()->where('var_atr_idAtributo = ?', $this->getRequest()->getParam('var_atr_idAtributo'))
					);
					if (null == $atributo) {
						throw new Exception('O atributo da varia��o solicitado n�o foi encontrada.');
					}
					try {
						$ordem = $atributo->getOrdem();
						$nome = $atributo->getValor();
						$variacao = $atributo->getVariacao();
						$atributo->delete();
						
						// Atualiza ordem dos atributos
						$proximas = $daoAtributos->fetchAll(
							$daoAtributos->select()
								->where('var_atr_idVariacao = ?', $variacao->getCodigo())
								->where('var_atr_ordem > ?', $ordem)
						);
						foreach ($proximas as $replaceAtributo) {
							$replaceAtributo->setOrdem($ordem);
							$replaceAtributo->save();
							$ordem++;
						}

						$retorno['success'] = true;
						$retorno['message'] = sprintf('Atributo da varia��o <b>%s</b> removido com sucesso.', $nome);
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover o atributo da varia��o.');
					}
				} catch (Exception $e) {
					$retorno['success'] = false;
					$retorno['message'] = $e->getMessage();
				}
				unset($daoAtributos, $atributo);

				App_Funcoes_UTF8::encode($retorno);
				echo Zend_Json::encode($retorno);
				break;
		}
	}
}