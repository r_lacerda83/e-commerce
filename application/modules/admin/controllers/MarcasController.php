<?php

class MarcasController extends Zend_Controller_Action
{
	public function init()
	{
		$this->getFrontController()->setParam('noViewRenderer', true);
		$this->getResponse()->setHeader('Content-Type', 'text/json');
	}

	public function indexAction()
	{
		switch ($this->getRequest()->getParam('load')) {
			case 'grid':
				$daoMarcas = App_Model_DAO_Marcas::getInstance();
				$daoProdutos = App_Model_DAO_Produtos::getInstance();

				$filter = $daoMarcas->select()
					->from($daoMarcas)
					->limit($this->getRequest()->getParam('limit', 30), $this->getRequest()->getParam('start', 0))
					->order("{$this->getRequest()->getParam('sort', 'marc_nome')} {$this->getRequest()->getParam('dir', 'ASC')}");
				App_Funcoes_Ext::FilterSQL($filter, $this->getRequest()->getParam('filter'));

				if ($this->getRequest()->getParam('excel', false) == true) {
					$this->exportExcel($filter);
				} else {
					$retorno = array('marcas' => array(), 'total' => 0);
					$rsMarcas = $daoMarcas->fetchAll($filter);
					foreach ($rsMarcas as $marca) {
						$retorno['marcas'][] = array(
							'marc_idMarca' => $marca->getCodigo(),
							'marc_tipo' => $marca->getTipo(),
							'marc_nome' => $marca->getNome(),
							'marc_produtos' => $daoProdutos->getAdapter()->fetchOne(
								$daoProdutos->select()->from($daoProdutos, 'COUNT(1)')
									->where('prod_idMarca = ?', $marca->getCodigo())
									->where('prod_status = ?', 1)
							),
							'marc_status' => (string) (int) $marca->getStatus()
						);
					}
					$retorno['total'] = $daoMarcas->getAdapter()->fetchOne(
						$daoMarcas->getAdapter()->select()
							->from(array('temp' => $filter->reset(Zend_Db_Select::LIMIT_COUNT)->reset(Zend_Db_Select::LIMIT_OFFSET)), 'COUNT(1)')
					);
					unset($rsMarcas);

					App_Funcoes_UTF8::encode($retorno);
					echo Zend_Json::encode($retorno);
				}
				unset($daoMarcas, $daoProdutos, $filter);
				break;

			default:
				$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
				$this->getFrontController()->setParam('noViewRenderer', false);
		}
	}

	public function insertAction()
	{
		if ($this->getRequest()->isPost()) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'marca' => array());
			$daoMarcas = App_Model_DAO_Marcas::getInstance();
			try {
				$marca = $daoMarcas->createRow();
				$marca = $this->montaObjeto($marca);
				
				try {
					$marca->save();

					$retorno['success'] = true;
					$retorno['message'] = sprintf('Marca <b>%s</b> cadastrada com sucesso.', $marca->getNome());
				} catch (App_Validate_Exception $e) {
					$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
					throw new Exception('Por favor verifique os campos marcados em vermelho.');
				} catch (Exception $e) {
					throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel cadastrar a marca.');
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoMarcas, $marca);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}

	public function updateAction()
	{
		if (false != ($idRegistro = $this->getRequest()->getParam('marc_idMarca', false))) {
			$retorno = array('success' => false, 'message' => null, 'errors' => array(), 'marca' => array());
			$daoMarcas = App_Model_DAO_Marcas::getInstance();
			try {
				$marca = $daoMarcas->fetchRow(
					$daoMarcas->select()->where('marc_idMarca = ?', $idRegistro)
				);
				if (null == $marca) {
					throw new Exception('A marca solicitada n�o foi encontrado.');
				}

				if ($this->getRequest()->getParam('load')) {
					// carrega os dados
					$retorno['success'] = true;
					$retorno['marca'] = array($marca->toArray());
				} else {
					// atualiza os dados
					$marca = $this->montaObjeto($marca);
					
					unset($daoArquivos);
					
					try {
						$marca->save();

						$retorno['success'] = true;
						$retorno['message'] = sprintf('Marca <b>%s</b> alterada com sucesso.', $marca->getNome());
					} catch (App_Validate_Exception $e) {
						$retorno['errors'] = App_Funcoes_Ext::fieldErrors($e);
						throw new Exception('Por favor verifique os campos marcados em vermelho.');
					} catch (Exception $e) {
						throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel alterar a marca.');
					}
				}
			} catch (Exception $e) {
				$retorno['success'] = false;
				$retorno['message'] = $e->getMessage();
			}
			unset($daoMarcas, $marca);

			App_Funcoes_UTF8::encode($retorno);
			echo Zend_Json::encode($retorno);
		} else {
			$this->getResponse()->setHeader('Content-Type', 'text/javascript', true);
			$this->render('form');
		}
	}
	
	private function montaObjeto(App_Model_Entity_Marca $marca) {
		$daoArquivos = App_Model_DAO_Galerias_Arquivos::getInstance();
		$marca->setNome(utf8_decode($this->getRequest()->getParam('marc_nome')))
			->setTipo($this->getRequest()->getParam('marc_tipo'))
			->setStatus($this->getRequest()->getParam('marc_status'));
		
		if (false != $this->getRequest()->getParam('marc_idLogo', false)) {
			$marca->setLogo(
					$daoArquivos->fetchRow(
							$daoArquivos->select()->where('gal_arq_idArquivo = ?', $this->getRequest()->getParam('marc_idLogo'))
					)
			);
		} else {
			$marca->setLogo(null);
		}
		
		unset($daoArquivos);
		return $marca;
		
	}
	
	public function deleteAction()
	{
		$retorno = array('success' => false, 'message' => null);
		$daoMarcas = App_Model_DAO_Marcas::getInstance();
		try {
			$marca = $daoMarcas->fetchRow(
				$daoMarcas->select()->where('marc_idMarca = ?', $this->getRequest()->getParam('idMarca'))
			);
			if (null == $marca) {
				throw new Exception('O marca solicitado n�o foi encontrado.');
			}
			try {
				$nome = $marca->getNome();
				$marca->delete();

				$retorno['success'] = true;
				$retorno['message'] = sprintf('Marca <b>%s</b> removida com sucesso.', $nome);
			} catch (Exception $e) {
				throw new Exception('development' == APPLICATION_ENV ? $e->getMessage() : 'N�o foi poss�vel remover a marca.');
			}
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		unset($daoMarcas, $marca);

		App_Funcoes_UTF8::encode($retorno);
		echo Zend_Json::encode($retorno);
	}
}