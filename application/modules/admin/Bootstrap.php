<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAutoload()
	{
		$loader = new Zend_Loader_Autoloader_Resource(array(
			'namespace' => 'App',
			'basePath'  => APPLICATION_ROOT,
			'resourceTypes' => array(
				'models' => array(
					'namespace' => 'Model',
					'path' => 'models/'
				),
				'plugins' => array(
					'namespace' => 'Plugin',
					'path' => 'modules/admin/plugins/'
				),
				'filters' => array(
					'namespace' => 'Filter',
					'path' => 'library/Filters/'
				),
				'validators' => array(
					'namespace' => 'Validate',
					'path' => 'library/Validators/'
				),
				'functions' => array(
					'namespace' => 'Funcoes',
					'path' => 'library/Funcoes/'
				),
			)
		));
		return $loader;
	}

	protected function _initRegistry()
	{
		$config = new Zend_Config(require_once APPLICATION_ROOT . '/configs/config.php');
		Zend_Registry::set('config', $config);
	}

	protected function _initFrontController()
	{
		$controller = Zend_Controller_Front::getInstance();
		$controller->setControllerDirectory(APPLICATION_PATH . '/controllers/')
			->registerPlugin(new App_Plugin_Auth())
			->throwExceptions(true);

		return $controller;
	}

	protected function _initView()
	{
		$view = new Zend_View();
		$view->doctype('XHTML1_STRICT');
		$view->setEncoding('ISO-8859-1');
		$view->setEscape('htmlentities');
		$view->HeadTitle(Zend_Registry::get('config')->project)->setSeparator(' - ');

		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer($view);
		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

		return $view;
	}

	function _initCache() {
		$config = Zend_Registry::get('config');
	
		if($config->memCache->ativo) {
			$frontendDriver = 'Core';
			$frontendOptions = array(
					'lifetime' => 7200, // cache lifetime of 2 hours
					'automatic_serialization' => true
			);
				
			$backendDriver = extension_loaded('memcache') ? 'Memcached' : 'File';
			$backendOptions = array(
					'servers' =>array($config->memCache->options->toArray()),
					'compression' => false
			);
	
			// getting a Zend_Cache_Core object
			$cache = Zend_Cache::factory($frontendDriver, $backendDriver, $frontendOptions, $backendOptions);
			Zend_Registry::set('memCache', $cache);
		}
	}
}