<?php

class UsuarioController extends Zend_Controller_Action
{
	public function identificacaoAction()
	{
		$this->view->HeadTitle('Identifica��o');

		$login = App_Plugin_Login::getInstance();
		if ($login->hasIdentity()) {
			$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base)->sendResponse();
		}

		//efetuar login
		if ($this->getRequest()->isPost()) {
			if ($login->authenticate($this->getRequest()->getParam('email'), $this->getRequest()->getParam('senha'))) {
				$carrinho = new Zend_Session_Namespace('carrinho');
				if (isset($carrinho->pedido) || ($carrinho->pedido instanceof App_Model_Entity_Pedido)) {
					$cliente = $login->getIdentity();
					if (!$carrinho->pedido->getEntrega()->getEndereco()->getCep()) {
						$carrinho->pedido->getEntrega()
							->getEndereco()
								->setCEP($cliente->getEndereco()->getCEP())
								->setLogradouro($cliente->getEndereco()->getLogradouro())
								->setNumero($cliente->getEndereco()->getNumero())
								->setComplemento($cliente->getEndereco()->getComplemento())
								->setBairro($cliente->getEndereco()->getBairro())
								->setCidade($cliente->getEndereco()->getCidade())
								->setUF($cliente->getEndereco()->getUF());
					}
				}
				
				$url = Zend_Registry::get('config')->paths->site->base;
				if ($this->getRequest()->getParam('redirect')) {
					$url = urldecode($this->getRequest()->getParam('redirect'));
				}
				$this->getResponse()->setRedirect($url)->sendResponse();
			} else {
				$this->view->message = "Usu�rio ou senha inv�lidos";
			}
		}

		$this->view->redirect = $this->getRequest()->getParam('redirect');
	}

	public function logoutAction()
	{
		$login = App_Plugin_Login::getInstance();
		if ($login->hasIdentity()) {
			$login->clearIdentity();
			Zend_Session::namespaceUnset('carrinho');
		}
		$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base)->sendResponse();
	}
	
	public function pedidosAction()
	{		
		$this->view->HeadTitle('Meus pedidos');

		$daoPedidos = App_Model_DAO_Pedidos::getInstance();
		$select = $daoPedidos->select()->where('ped_idCliente = ?', App_Plugin_Login::getInstance()->getIdentity()->getCodigo())->order('ped_data DESC');
				
		$this->view->pedidos = $daoPedidos->fetchAll($select);
	}	
}