<?php

class ProdutosController extends Zend_Controller_Action
{
	public function indexAction()
	{	
		$palavra = $this->getRequest()->getParam('palavra', '');
		$this->view->palavra = $palavra;
		$this->view->solr = false;
		
		//Verificando se o Solr esta ativo
		$config = Zend_Registry::get('config');

		// verifica se Solr esta ativo na aplicacao e se o servidor esta acessivel
		if($config->solr->ativo) {
			try {
				$client = new SolrClient($config->solr->options->toArray());
				$client->ping();
				$carregarSolr = true;
			} catch (Exception $e) {
				$carregarSolr = false;
			}
		}
		
		if($carregarSolr) {
			$queryParams = "";
			
			$queryParams = $palavra ? "text:({$palavra}~)" : '*:*';
			if (false != ($idCategoria = $this->getRequest()->getParam('idCategoria', false))) {
				$queryParams .= " AND cat:({$idCategoria})";
			}
			
			$query = new SolrQuery(utf8_encode($queryParams));
			
			$query->setStart(0);
			$query->setRows(30);
			
			$query->addField('id')
				->addField('sku')
				->addField('preco')
				->addField('imagem')
				->addField('nome');
			
			$queryResponse = $client->query($query);
			$response = $queryResponse->getResponse();
			
			$this->view->produtos = $queryResponse->getResponse();
			$this->view->solr = true;
			
		} else {
			$daoProdutos = App_Model_DAO_Produtos::getInstance();
			$daoSKU = App_Model_DAO_Produtos_SKU::getInstance();
			$daoMarcas = App_Model_DAO_Marcas::getInstance();
			$daoCategoriaProduto = App_Model_DAO_Produtos_Categorias::getInstance();
			$daoCategoria = App_Model_DAO_Categorias::getInstance();
			
			$selectProduto = $daoProdutos->getAdapter()->select()
			->from($daoProdutos->info('name'))
			->joinInner($daoSKU->info('name'), 'prod_sku_idProduto = prod_idProduto')
			->joinInner($daoMarcas->info('name'), 'marc_idMarca = prod_idMarca', null)
			->joinInner($daoCategoriaProduto->info('name'), 'prod_idProduto = prod_cat_idProduto', null)
			->joinInner($daoCategoria->info('name'), 'prod_cat_idCategoria = cat_idCategoria', null)
			->where('prod_status = ?', 1)
			->group('prod_idProduto');
			
			if($palavra != null) $selectProduto->where('prod_nome LIKE ? OR prod_tags LIKE ? OR prod_descricao LIKE ? OR marc_nome LIKE ? OR cat_nome LIKE ?', "%{$palavra}%");
				
			$daoCategoria = App_Model_DAO_Categorias::getInstance();
			if (false != ($idCategoria = $this->getRequest()->getParam('idCategoria', false))) {
				$selectProduto->where('cat_idCategoria = ?', $idCategoria);
				if($daoCategoria->find($idCategoria)->count()) {
					$this->view->categoria = $daoCategoria->find($idCategoria)->current();
				}
			}
			
			$paginator = new Zend_Paginator(
					new Zend_Paginator_Adapter_DbSelect($selectProduto)
			);
			$paginator->setItemCountPerPage(12);
			$paginator->setPageRange(5);
			$paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
			$this->view->paginator = $paginator;
			
			unset($daoProdutos, $daoMarcas, $daoCategoriaProduto, $daoCategoria);
		}
		
	}

	private function getProduto()
	{
		$daoProdutos = App_Model_DAO_Produtos::getInstance();
		$produto = $daoProdutos->fetchRow(
				$daoProdutos->select()
				->where('prod_idProduto = ?', $this->getRequest()->getParam('codigo'))
				->where('prod_status = ?', 1)
		);
		if (null == $produto) {
			$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base)->sendResponse();
		}
		return $produto;
	}
	
	public function detalheAction()
	{
		$this->view->HeadScript()->appendFile('js/jquery.maskedinput.js');
		if($this->getRequest()->getParam('codigo', false) != false) {

			$config = Zend_Registry::get('config');

			// verifica se Solr esta ativo na aplicacao e se o servidor esta acessivel
			if($config->memCache->ativo) {
				$cache = Zend_Registry::get('memCache');
				if(!$produto = $cache->load('prod' . $this->getRequest()->getParam('codigo', false))) {
					$produto = $this->getProduto();
					
					$cache->save($produto, 'prod' . $produto->getCodigo(), array('produtos'), 3600);
				}
			} else {
				$produto = $this->getProduto();
			}
					
			$this->view->catBread = $produto->getCategorias(true)->seek(0)->current();
			$this->view->HeadTitle($produto->getMetaTitle() ? $produto->getMetaTitle() : $produto->getNome());
				
			if ($produto->getMetaTags()) {
				$this->view->HeadMeta()->appendName('keywords', $produto->getMetaTags());
			}
			if ($produto->getMetaDescription()) {
				$this->view->HeadMeta()->appendName('description', $produto->getMetaDescription());
			}
				
			// Define a vitrine
			$sku = $this->getRequest()->getParam('sku', $produto->getVitrine()->getSku());
			foreach ($produto->getSku() as $vitrine) {
				if($vitrine->getSku() == $sku) {
					$this->view->vitrine = $vitrine;
					break;
				}
			}
		
			$this->view->produto = $produto;
			$this->createRecommendationQueue($this->view->vitrine);
			$recommendations = new App_Services_Recommendations_Gremlin();
			$this->view->viewAlsoView = $recommendations->getViewAlsoView($this->view->vitrine);
		} else {
			$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base)->sendResponse();
		}		
		
	}

	private function createRecommendationQueue(App_Model_Entity_Produto_SKU $sku)
    {
        $login = App_Plugin_Login::getInstance();
        $recommendationQueue = new App_Services_Recommendations_QueueManagement();
        $recommendationQueue->setUser($login->hasIdentity() ? $login->getIdentity() : null);
        $recommendationQueue->setSku($sku);
        $recommendationQueue->setAction(App_Services_Gearman_Queue_Recommendation::ACTION_VIEW);
		$recommendationQueue->createQueue();
	}
}