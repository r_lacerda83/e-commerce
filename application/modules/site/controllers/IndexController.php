<?php

class IndexController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$this->view->HeadTitle('Bem vindo');
		
		$palavra = $this->getRequest()->getParam('palavra', '');
		$this->view->palavra = $palavra;
		
		$daoProdutos = App_Model_DAO_Produtos::getInstance();
		$daoSKU = App_Model_DAO_Produtos_SKU::getInstance();
		$daoMarcas = App_Model_DAO_Marcas::getInstance();
		$daoCategoriaProduto = App_Model_DAO_Produtos_Categorias::getInstance();
		$daoCategoria = App_Model_DAO_Categorias::getInstance();
		
		$selectProduto = $daoProdutos->getAdapter()->select()
			->from($daoProdutos->info('name'))
			->joinInner($daoSKU->info('name'), 'prod_sku_idProduto = prod_idProduto')
			->joinInner($daoMarcas->info('name'), 'marc_idMarca = prod_idMarca', null)
			->joinInner($daoCategoriaProduto->info('name'), 'prod_idProduto = prod_cat_idProduto', null)
			->joinInner($daoCategoria->info('name'), 'prod_cat_idCategoria = cat_idCategoria', null)
			->where('prod_status = ?', 1)
			->where('prod_destaqueHome = ?', 1)
			->order('rand()')
			->limit(6)
			->group('prod_idProduto');
		
		
		$this->view->produtos = $daoProdutos->createRowset($daoProdutos->getAdapter()->fetchAll($selectProduto));
		unset($daoProdutos, $daoMarcas, $daoCategoriaProduto, $daoCategoria);
	}
	
}