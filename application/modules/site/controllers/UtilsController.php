<?php

class UtilsController extends Zend_Controller_Action
{
	public function init()
	{
		Zend_Layout::getMvcInstance()->disableLayout();
		$this->getFrontController()->setParam('noViewRenderer', true);
	}

	public function buscaEnderecoAction()
	{
		$this->getResponse()->setHeader('Content-Type', 'text/json');

		if (($cep = $this->getRequest()->getParam('cep', false)) != false) {
			$endereco = App_Funcoes_Correios::buscaEndereco($cep);
			App_Funcoes_UTF8::encode($endereco);
			echo Zend_Json::encode($endereco);
		}
	}
	
	public function resizeAction()
	{
		if ($this->getRequest()->getParam('imagem', null) != null) {
			$imagem = $this->getRequest()->getParam('imagem');
			$width = $this->getRequest()->getParam('width');
			$height = $this->getRequest()->getParam('height');

			require_once 'resizeGD.php';
			$resizeGD = new resizeGD($imagem);
			$resizeGD->resizeToMaxWidthHeight($width, $height);
			$resizeGD->show();
		}
	}
}