<?php

class CarrinhoController extends Zend_Controller_Action
{
	/**
	 * @var App_Model_Entity_Pedido
	 */
	protected $pedido;

	public function init()
	{
		$carrinho = new Zend_Session_Namespace('carrinho');
		if (!isset($carrinho->pedido) || !($carrinho->pedido instanceof App_Model_Entity_Pedido)) {
			$carrinho->pedido = App_Model_DAO_Pedidos::getInstance()->createRow();
		}
		$this->pedido = $carrinho->pedido;
	}

	public function indexAction()
	{
		$this->view->HeadTitle('Carrinho');
		$this->view->HeadScript()->appendFile('js/carrinho.js');
		$this->view->HeadScript()->appendFile('js/jquery.maskedinput.js');
		
		
		if ($this->getRequest()->isPost()) {
			//atualiza as quantidades
			$quantidades = $this->getRequest()->getParam('quantidade', array());
			$messages = array();
			if (is_array($quantidades)) {
				foreach ($quantidades as $sku => $qtde) {
					$itemPedido = $this->pedido->getItems()->find('ped_item_sku', $sku);
					if (null != $itemPedido) {
						if ((int) $qtde > 0) {
							if ($itemPedido->getQuantidade() != $qtde) {
								$itemPedido->setQuantidade($qtde);
							}
						} else {
							$this->pedido->getItems()->remove($itemPedido);
						}
					}
				}
			}

			//calcula as op��es de frete
			if (false != ($cep = $this->getRequest()->getParam('cep', false))) {				
				if ($cep != $this->pedido->getEntrega()->getEndereco()->getCEP()) {
					$cep = $this->getRequest()->getParam('cep');
					$endereco = App_Funcoes_Correios::buscaEndereco($cep);					
					if (!$endereco['erro_descricao']) {
												
						$this->pedido->getEntrega()->getEndereco()->setCEP($cep)
							->setLogradouro($endereco['endereco'])
							->setNumero(null)
							->setComplemento(null)
							->setBairro($endereco['bairro'])
							->setCidade($endereco['cidade'])
							->setUF($endereco['estado']);
						$this->pedido->getEntrega()->setServico(null);
					}
				}
			} else {
				$this->pedido->getEntrega()->setServico(null);
				$this->pedido->getEntrega()->getEndereco()->setCEP(null)->setLogradouro(null)->setNumero(null)->setComplemento(null)->setBairro(null)->setCidade(null)->setUF(null);
			}

			//forma de entrega
			if (false != ($frete = $this->getRequest()->getParam('entrega'))) {
				$servico = false;
				foreach ($this->pedido->getEntrega()->getFormasDisponiveis() as $forma) {
					foreach ($forma->getServicosDisponiveis() as $item) {
						if ($item->getCodigo() == $frete) {
							$servico =& $item;
							break;
						}
					}
				}
				if (false !== $servico) {
					$this->pedido->getEntrega()->setServico($servico);
				} else {
					$this->pedido->getEntrega()->setServico(null);
				}
			}
			
			//fechamento solicitado
			if ($this->getRequest()->getParam('fechamento')) {
				$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base.'/carrinho/fechamento')->sendResponse();
			}			
		}

		$this->view->pedido = $this->pedido;
	}

	public function adicionarAction()
	{
		$itemPedido = null;

		if (false != ($sku = $this->getRequest()->getParam('sku', false))) {
			$qtd = (int) $this->getRequest()->getParam('quantidade', 1);
			if (!$this->pedido->getItems()->find('ped_item_sku', $sku)) {
				$daoProdutos = App_Model_DAO_Produtos::getInstance();
				$daoProdutosSKU = App_Model_DAO_Produtos_SKU::getInstance();
				$daoMarcas = App_Model_DAO_Marcas::getInstance();

				$itemSKU = $daoProdutosSKU->getAdapter()->fetchRow(
					$daoProdutosSKU->getAdapter()->select()
						->from($daoProdutosSKU->info('name'))
						->joinInner($daoProdutos->info('name'), 'prod_idProduto = prod_sku_idProduto AND prod_status = 1', null)
						->joinInner($daoMarcas->info('name'), 'marc_idMarca = prod_idMarca AND marc_status = 1', null)
						->where('prod_sku_SKU = ?', $sku)
						->where('prod_sku_status = ?', 1)
				);

				if (null == $itemSKU) {
					$this->view->message = 'O produto especificado n�o foi encontrado.';
				} else if($qtd) {
					$itemSKU = $daoProdutosSKU->createRow($itemSKU);
					$itemPedido = App_Model_DAO_Pedidos_Items::getInstance()->createRow();
					$itemPedido->setPedido($this->pedido)
						->setProduto($itemSKU)
						->setQuantidade($qtd);

					$this->pedido->getItems()->add($itemPedido);
					$this->view->message = "Produto <strong>{$itemSKU->getProduto()->getNome()}</strong> adicionado ao carrinho.";
				}

				unset($daoProdutos, $daoProdutosSKU, $daoMarcas, $itemSKU);
			}
		}

		$this->_forward('index');
	}
	
	public function removerAction()
	{
		$sku = $this->getRequest()->getParam('item');
		if (strlen($sku)) {
			$item = $this->pedido->getItems()->find('ped_item_sku', $sku);
			if (null != $item) {
				$this->pedido->getItems()->remove($item);
			}
		}

		$this->_forward('index');
	}
	
	public function fechamentoAction()
	{
		if (!$this->pedido->getItems()->count()) {
			$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base.'/meus-pedidos')->sendResponse();
			die();
		}

		$this->view->HeadTitle('Confira seu Pedido');
		$this->view->HeadScript()->appendFile('js/carrinho.js');

		//cliente
		if (!$this->pedido->getCliente()) {
			$this->pedido->setCliente(clone App_Plugin_Login::getInstance()->getIdentity());
		}

		if ($this->getRequest()->isPost()) {
			$clonePedido = serialize($this->pedido);

			//salva o pedido
			try {	
				$cliente = $this->pedido->getCliente();
				$this->pedido->getEntrega()
					->getEndereco()
						->setCEP($cliente->getEndereco()->getCEP())
						->setLogradouro($cliente->getEndereco()->getLogradouro())
						->setNumero($cliente->getEndereco()->getNumero())
						->setComplemento($cliente->getEndereco()->getComplemento())
						->setBairro($cliente->getEndereco()->getBairro())
						->setCidade($cliente->getEndereco()->getCidade())
						->setUF($cliente->getEndereco()->getUF());
				
				//verifica o estoque dos itens
				$daoSKU = App_Model_DAO_Produtos_SKU::getInstance();
				foreach ($this->pedido->getItems() as $item) {
					$estoque = $daoSKU->getAdapter()->fetchOne(
						$daoSKU->select()->from($daoSKU, 'prod_sku_estoque')
							->where('prod_sku_SKU = ?', $item->getProduto()->getSKU())
					);					

					if ($estoque < $item->getQuantidade()) {						
						throw new Exception("N�o temos esta quantidade de {$item->getProduto()->getNome(true)} escolhida. Tente reduzir a quantidade para continuar comprando.", -1);
					}
				}
				unset($daoSKU);

				try {		
					App_Model_DAO_Pedidos::getInstance()->getAdapter()->query('SET SESSION wait_timeout = 100');								
					$this->pedido->getTable()->getAdapter()->beginTransaction();						
					$this->pedido->save();
				
					try {
						$daoPedidos = App_Model_DAO_Pedidos::getInstance();
						$dadosPedido = $daoPedidos->fetchRow(
							$daoPedidos->select()->where('ped_idPedido = ?', $this->pedido->getCodigo())
						);
						
						//App_Model_DAO_Pedidos::getInstance()->sendEmailPedido($dadosPedido);
						unset($daoPedidos, $dadosPedido);
						
					} catch (Exception $e) {}
				} catch (App_Validate_Exception $e) {
					$lista = array();
					$campos = array(
						'ped_ent_tipoEndereco' => '- No endere�o de entrega o CEP de entrega deve ser informado',
						'ped_ent_endereco' => '- No endere�o de entrega o CEP de entrega deve ser informado',
						'ped_ent_logradouro' => '- No endere�o de entrega o Logradouro de entrega deve ser informado',
						'ped_ent_numero' => '- No endere�o de entrega o N�mero do endere�o de entrega deve ser informado',
						'ped_ent_cep' => '- No endere�o de entrega o CEP de entrega deve ser informado',
						'ped_ent_bairro' => '- No endere�o de entrega o Bairro de entrega deve ser informado',
						'ped_ent_cidade' => '- No endere�o de entrega a Cidade de entrega deve ser informado',
						'ped_ent_uf' => '- No endere�o de entrega o UF de entrega deve ser informado',
						'ped_ent_forma' => '- A forma de entrega n�o foi especificada',
						'ped_ent_servico' => '- O servi�o de entrega n�o foi especificado',
						'ped_ent_valor' => '- O c�digo do servi�o de entrega n�o foi especificado'
					);
					foreach ($e->getFields() as $field => $message) {
						if (in_array($field, array_keys($campos))) {
							if($field != 'ped_ent_forma' || $field != 'ped_ent_servico' || $field != 'ped_ent_valor') {
								$message = $campos[$field];
							}
						}
						$lista[] = $message;
					}
					throw new Exception("N�o foi poss�vel salvar o pedido.<br />".implode(', <br />', $lista));
				} catch (Exception $e) {
					throw new Exception($e->getMessage());
				}

				$this->pedido->getTable()->getAdapter()->commit();
				$idPedido = $this->pedido->getCodigo();
				Zend_Session::namespaceUnset('carrinho');
					
				$this->getResponse()->setRedirect(Zend_Registry::get('config')->paths->site->base."/meus-pedidos")->sendResponse();
			} catch (Exception $e) {
				if($e->getCode() >= 0) $this->pedido->getTable()->getAdapter()->rollBack();
				Zend_Session::namespaceUnset('carrinho');
				$carrinho = new Zend_Session_Namespace('carrinho');
				$carrinho->pedido = unserialize($clonePedido);
				$this->view->message = $e->getMessage();
			}
		} else {
			$this->view->message = $this->getRequest()->getParam('erro', '');
		}

		$this->view->pedido = $this->pedido;
	}
}