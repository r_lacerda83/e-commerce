<?php

class App_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
	// controladores e a��es que requer login
	private $auth = array(
		array(
			'controller' => 'usuario',
			'action' => 'pedidos'
		),
		array(
			'controller' => 'carrinho',
			'action' => 'fechamento'
		)		
	);

	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{		
		$login = App_Plugin_Login::getInstance();
		foreach ($this->auth as $nfo) {
			if ($nfo['controller'] == $request->getControllerName() && $nfo['action'] == $request->getActionName()) {
				$url = Zend_Registry::get('config')->paths->site->base.'/identificacao/';
				if($request->getControllerName() == 'carrinho') {
					$url = Zend_Registry::get('config')->paths->site->base.'/identificacao/show/true';
				}
				if (!$login->hasIdentity()) {
					$this->getResponse()->setHttpResponseCode(401)
						->setRedirect("{$url}?redirect=".urlencode($_SERVER['REQUEST_URI']))
						->sendResponse();
					die;
				} else {
					break;
				}
			}
		}
		unset($login);
	}
}