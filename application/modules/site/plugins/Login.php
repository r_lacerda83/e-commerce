<?php

class App_Plugin_Login
{
	protected static $instance = null;

	protected $storageName = 'Nobel_Login_Site';
	protected $auth;

	public function __construct()
	{
		$this->auth = Zend_Auth::getInstance()->setStorage(
			new Zend_Auth_Storage_Session($this->storageName)
		);
	}

	/**
	 * Implementa��o do m�todo Singleton para obter a instancia da classe
	 *
	 * @return App_Plugin_Login
	 */
	static public function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
        }
		return self::$instance;
	}

	/**
	 * Autentica um usu�rio
	 *
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 */
	public function authenticate($username, $password)
	{
		$daoClientes = App_Model_DAO_Clientes::getInstance();
		$cliente = $daoClientes->fetchRow(
			$daoClientes->select()->where('cli_email = ?', $username)
				->where('cli_senha = ?', $password)
				->where('cli_status = ?', 1)
		);
		if (null != $cliente) {
			$this->setIdentity($cliente);
			return true;
		} else {
			return false;
		}
	}

	public function clearIdentity()
	{
		if ($this->auth->hasIdentity()) {
			$this->auth->clearIdentity();
		} else {
			throw new Exception('Nenhum usu�rio logado');
		}
	}

	public function setIdentity(App_Model_Entity_Cliente $value)
	{
		$this->auth->getStorage()->write(serialize($value));
	}

	/**
	 * Recupera o usu�rio logado
	 *
	 * @return App_Model_Entity_Cliente
	 * @throws Exception
	 */
	public function getIdentity()
	{
		if ($this->auth->hasIdentity()) {
			$identity = unserialize($this->auth->getStorage()->read());
			$identity->setTable(App_Model_DAO_Clientes::getInstance());
			return $identity;
		} else {
			throw new Exception('Nenhum usu�rio logado');
		}
	}

	/**
	 * Verifica se existe algum usu�rio logado
	 *
	 * @return boolean
	 */
	public function hasIdentity()
	{
		return $this->auth->hasIdentity();
	}
}