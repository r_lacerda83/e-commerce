<?php

class Zend_View_Helper_MinhaSacola extends Zend_View_Helper_Abstract
{
	private $carrinho = null;
	
	/**
	 * @return App_Model_Entity_Pedido_Items
	 */
	public function MinhaSacola()
	{
		$this->carrinho = new Zend_Session_Namespace('carrinho');
		return $this;
	}
	
	public function qtde() {
		$count = 0;
		if (isset($this->carrinho->pedido) || ($this->carrinho->pedido instanceof App_Model_Entity_Pedido)) {
			foreach ($this->carrinho->pedido->getItems() as $item) {
				$count += $item->getQuantidade();
			}
			return $count;
		} else {
			return 0;
		}
	}
	
	public function getItems() {
		if (isset($this->carrinho->pedido) || ($this->carrinho->pedido instanceof App_Model_Entity_Pedido)) {
			return $this->carrinho->pedido->getItems();			
		} else {
			return array();
		}
	}	
}