<?php

class Zend_View_Helper_Categorias extends Zend_View_Helper_Abstract
{
	public function Categorias() {
		return $this;
	}
	
	/**
	 * Recupera todas as categorias pais para exibir no topo da p�gina
	 *
	 * @return App_Model_Collection of App_Model_Entity_Categoria
	 */
	public function getListagem($idCategoria = false)
	{
		$daoCategorias = App_Model_DAO_Categorias::getInstance();
		$select = $daoCategorias->select()->from($daoCategorias)
				->where('cat_status = ?', 1)
				->order('cat_nome ASC'); 
		
		if($idCategoria) {
			$select->where('cat_idCategoriaPai = ?', $idCategoria);
		} else {
			$select->where("isNULL(cat_idCategoriaPai)");
		}
		
		return $daoCategorias->fetchAll($select);
		
	}
	
	/**
	 * @param App_Model_Entity_Categoria $categorias
	 * @return App_Model_Entity_Categoria
	 */
	public function getPaiPrincipal(App_Model_Entity_Categoria $categoria) {
		$temp = $categoria;
		while ($temp->getPai() != null) {
			$temp = $temp->getPai();
		}
		return $temp;
	}
}
