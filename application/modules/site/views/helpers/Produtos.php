<?php

class Zend_View_Helper_Produtos extends Zend_View_Helper_Abstract
{
	public function Produtos() {
		return $this;
	}
	
	public function renderProduto(App_Model_Entity_Produto $produto) {
		$view = Zend_Layout::getMvcInstance()->getView();
		$view->produto = $produto;
		
		echo $view->render('produtos/bloco-produto.phtml');
	}
	
	public function renderProdutosSolr($listaProdutos) {
		$view = Zend_Layout::getMvcInstance()->getView();
		$view->produtos = $listaProdutos;
	
		echo $view->render('produtos/bloco-produto-solr.phtml');
	}
}
