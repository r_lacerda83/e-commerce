<?php

class App_Funcoes_Rotulos
{
	public static $status = array('1' => 'Ativo', '0' => 'Inativo');
	
	public static $formaEnvio = array('SMTP' => 'SMTP', 'MAIL' => 'PHP MAIL');
	
	public static $tipoEndereco = array('R' => 'Residencial', 'C' => 'Comercial');

	public static $statusSimNao = array('1' => 'Sim', '0' => 'N�o');
	
	public static $tipoMarca = array('N' => 'Nacional', 'I' => 'Importada');
	
	public static $sexo = array('M' => 'Masculino', 'F' => 'Feminino');

	public static $UF = array(
		'AC' => 'Acre',
		'AL' => 'Alagoas',
		'AM' => 'Amazonas',
		'AP' => 'Amap�',
		'BA' => 'Bahia',
		'CE' => 'Cear�',
		'DF' => 'Distrito Federal',
		'ES' => 'Esp�rito Santo',
		'GO' => 'Goi�s',
		'MA' => 'Maranh�o',
		'MG' => 'Minas Gerais',
		'MS' => 'Mato Grosso do Sul',
		'MT' => 'Mato Grosso',
		'PA' => 'Par�',
		'PB' => 'Para�ba',
		'PE' => 'Pernambuco',
		'PI' => 'Piau�',
		'PR' => 'Paran�',
		'RJ' => 'Rio de Janeiro',
		'RN' => 'Rio Grande do Norte',
		'RO' => 'Rond�nia',
		'RR' => 'Roraima',
		'RS' => 'Rio Grande do Sul',
		'SC' => 'Santa Catarina',
		'SE' => 'Sergipe',
		'SP' => 'S�o Paulo',
		'TO' => 'Tocantins'
	);

	public static $statusPedido = array('1'=>'Aguardando Pagamento', '2'=>'Aguardando Confirma��o', '3'=>'Pagamento Confirmado', '4'=>'Pedido Enviado', '5'=>'Cancelado', '6'=> 'Pedido Entregue');

	public static $meses =  array(
		'01' => 'Janeiro',
		'02' => 'Fevereiro',
		'03' => 'Mar�o',
		'04' => 'Abril',
		'05' => 'Maio',
		'06' => 'Junho',
		'07' => 'Julho',
		'08' => 'Agosto',
		'09' => 'Setembro',
		'10' => 'Outubro',
		'11' => 'Novembro',
		'12' => 'Dezembro'
	);

	public static $conhecimento = array(
		'1' => 'Revistas',
		'2' => 'Jornais',
		'3' => 'Sites de busca',
		'4' => 'R�dio',
		'5' => 'Televis�o',
		'6' => 'Amigos',
		'7' => 'Outros'
	);
}