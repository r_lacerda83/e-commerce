<?php

class App_Funcoes_Busca
{
	/**
	 * Gera o link para a p�gina atual, colocando a pagina��o
	 *
	 * @param int $pagina
	 * @param array $params
	 * @param string $paginatorVar
	 * @return string
	 */
	public static function linkPaginacao($pagina = null, array $params = null, array $ignore = null, $paginatorVar = 'p')
	{
		$queryString = '';

		//qual � a p�gina que est� sendo paginada
		$urlAtual = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?'));
		if (!$urlAtual) $urlAtual = $_SERVER['REQUEST_URI'];
		if(substr($urlAtual, strlen($urlAtual)-1, 1) != '/') {
			$urlAtual .= '/';
		}
		$urlAtual = "http://{$_SERVER['HTTP_HOST']}". htmlentities($urlAtual);

	    //link da p�gina atual com toda query string
	    $queryParams = array();
	 	foreach($_GET as $k => $v) {
	 		if (($k <> $paginatorVar) && (!is_array($params) || !in_array($k, array_keys($params)))) {
	 			if ($ignore == null || (is_array($ignore) && !in_array($k, $ignore))) {
					$queryParams[] = sprintf("%s=%s", urlencode(htmlentities($k)), urlencode($v));
	 			}
	 		}
	 	}

	 	if (null != $pagina) {
	 		$queryParams[] = "{$paginatorVar}={$pagina}";
	 	}

	 	if (null != $params && count($params)) {
	 		foreach ($params as $k => $v) {
	 			$queryParams[] = sprintf("%s=%s", htmlentities(urldecode($k)), htmlentities(urldecode($v)));
	 		}
	 	}

	 	$queryString = implode('&', $queryParams);
	 	if (strlen($queryString)) $queryString = "?{$queryString}";

		return "{$urlAtual}{$queryString}";
	}
}