<?php

class App_Funcoes_String
{
	public static function cortaTexto($word, $text, $maxlength=300)
	{
		$word = html_entity_decode(strip_tags($word));
		$text = html_entity_decode(strip_tags($text));
		if (strlen($text) > $maxlength) {
			$pos_word = strpos(strtoupper($text), strtoupper($word));

			// Procura por quebra de linha antes da posi��o da palavra procurada
			if ($pos_word-($pi = strrpos(substr($text, 0, $pos_word), 13)) < $maxlength) {
				$text = substr($text, $pi, $maxlength);
			// Procura in�cio de par�grafo antes da posi��o da palavra procurada
			} elseif ($pos_word-($pi = strrpos(substr($text, 0, $pos_word), '.')+1) < $maxlength) {
				$text = substr($text, $pi, $maxlength);
			// nada encontrado, inicia o resultado com a metade de caracteres em $maxlength
			} else {
				$text = substr($text, $pos_word-($maxlength/2), $maxlength);
				$text = '...' . substr($text, strpos($text, ' '));
			}

			// Verifica final de par�grafo ap�s a palavra procurada
			if ($pe = strrpos($text, '.')) {
				$text = substr($text, 0, $pe+1);
			} elseif ($pe = strrpos($text, 13)) {
				$text = substr($text, 0, $pe);
			} else {
				$text = substr($text, 0, strrpos($text, ' ')) . '...';
			}
		}
		return (trim(preg_replace("/$word/i", "<b>{$word}</b>", $text)));
	}
}