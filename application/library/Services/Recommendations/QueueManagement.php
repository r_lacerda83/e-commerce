<?php

class App_Services_Recommendations_QueueManagement
{

    /**
     * @var App_Model_Entity_Produto_SKU
     */
    protected $sku;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var App_Model_Entity_Cliente
     */
    protected $user;

    /**
     * @return App_Model_Entity_Produto_SKU
     */
    protected function getSku()
    {
        return $this->sku;
    }

    /**
     * @param App_Model_Entity_Produto_SKU $sku
     * @return $this
     */
    public function setSku(App_Model_Entity_Produto_SKU $sku)
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @param App_Model_Entity_Cliente $user
     */
    public function setUser(App_Model_Entity_Cliente $user = null)
    {
        $this->user = $user;
    }

    /**
     * @return App_Model_Entity_Cliente
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    public function createQueue()
    {
        if(!$this->sku) {
            throw new Exception('SKU is required for this operation');
        }

        if(!$this->action) {
            throw new Exception('Action is required for this operation');
        }

        $arrayRequest = [];
        $arrayRequest['user'] = array(
            'uid' => $this->user ? $this->user->getCodigo() : 0,
            'name' => $this->user ? $this->user->getNome() : 'Visitor',
            'session_id' => session_id()
        );

        $arrayRequest['sku'] = array(
            'sku' => $this->sku->getSKU(),
            'name' => $this->sku->getProduto()->getNome()
        );

        $arrayRequest['action'] = $this->getAction();

        $queueRecommendation = App_Services_Gearman_Queue_Recommendation::getInstance();
        $responseHandle = $queueRecommendation->handleRequest($arrayRequest);
        if(!$responseHandle['success']) {
            throw new Exception($responseHandle['message']);
        }

        $jsonRequest = Zend_Json::encode($arrayRequest);
        $queueRecommendation->createAsyncJob($jsonRequest);
    }

}