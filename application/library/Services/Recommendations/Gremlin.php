<?php

class App_Services_Recommendations_Gremlin extends \brightzone\rexpro\Connection
{

    public function __construct()
    {
        parent::__construct();

        $config = Zend_Registry::get('config')->gremlin;
        if ($config->enabled) {
            $this->open($config->host . ':' . $config->port);
        }
    }

    public function removeAllVertex()
    {

    }

    /**
     * @param App_Model_Entity_Produto_SKU $sku
     * @return array
     * @throws Exception
     * @throws \brightzone\rexpro\ServerException
     */
    public function getViewAlsoView(App_Model_Entity_Produto_SKU $sku)
    {
        $command = "g.V('sku', '{$sku->getSKU()}').in('view').out('view').groupCount().sort{-it.value}";
        $result = $this->send($command);
        $this->close();
        return $result;
    }

    public function getViewBougth()
    {

    }

    public function getItemsSimiliraty()
    {

    }


}