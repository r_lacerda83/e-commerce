<?php

abstract class App_Services_Gearman_Queue_Abstract
{
	protected $client = null;
    protected $function;

	const PRIORITY_LOW = 0;
	const PRIORITY_MEDIUM = 1;
	const PRIORITY_HIGH = 2;

	public function __construct()
	{
		if (!extension_loaded('gearman')) {
			throw new RuntimeException('The PECL::gearman extension is required.');
		}
		
		$client = new GearmanClient();
		$client->addServer(Zend_Registry::get('config')->gearman->host);
		$this->client = $client;
	}
	
	public function createSyncJob($data, $priority = self::PRIORITY_MEDIUM)
	{
		switch ($priority) {
			case self::PRIORITY_LOW:
				$this->client->doLow($this->getFunction(), $data);
				break;

			case self::PRIORITY_MEDIUM:
				$this->client->doNormal($this->getFunction(), $data);
				break;

			case self::PRIORITY_HIGH:
				$this->client->doHigh($this->getFunction(), $data);
				break;
		}
	}
	
	public function createAsyncJob($data, $priority = self::PRIORITY_MEDIUM)
	{
		switch ($priority) {
			case self::PRIORITY_LOW:
				$this->client->doLowBackground($this->getFunction(), $data);
				break;

			case self::PRIORITY_MEDIUM:
				$this->client->doBackground($this->getFunction(), $data);
				break;

			case self::PRIORITY_HIGH:
				$this->client->doHighBackground($this->getFunction(), $data);
				break;
		}

		if ($this->client->returnCode() != GEARMAN_SUCCESS)
		{
			throw new RuntimeException('Process job error.' . $this->client->returnCode());
		}
	}

    /**
     * @param $function
     */
    public function setFunction($function)
    {
        $this->function = $function;
    }

    /**
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }
	
	public function getPathJsonSchemas() 
	{
		return APPLICATION_ROOT . '/library/Services/Gearman/Queue/JsonSchemas/';
	}
}