<?php

class App_Services_Gearman_Queue_Recommendation extends App_Services_Gearman_Queue_Abstract
{
	const ACTION_VIEW = 'view';
	const ACTION_BOUGHT = 'bought';
	const ACTION_RATED = 'rated';
	const WORKER_FUNCTION = 'recommendation';

	protected static $instance = null;
	protected $jsonSchema = 'recommendation.json';

	public function __construct()
	{
		parent::__construct();
		$this->setFunction(self::WORKER_FUNCTION);
	}

    /**
     * @return App_Services_Gearman_Queue_Recommendation|null
     */
	public static function getInstance()
	{
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

    /**
     * @param array $arrayRequest
     * @return mixed
     */
	public function handleRequest(array $arrayRequest)
	{
        $obj = json_decode(json_encode($arrayRequest), FALSE);
		$retorno['success'] = true;
		try {
			$validatorJson = new App_Services_JsonValidator($this->getJsonSchema());
			$validatorJson->validate($obj);
		} catch (Exception $e) {
			$retorno['success'] = false;
			$retorno['message'] = $e->getMessage();
		}
		
		return $retorno;
	}

    /**
     * @return string
     */
	public function getJsonSchema()
	{
		return $this->getPathJsonSchemas() . $this->jsonSchema;
	}
}