<?php

class App_Services_Gearman_Worker_Recommendation extends App_Services_Gearman_Worker_Abstract
{
    public function __construct()
    {
        $this->worker->addFunction("send_email", function(GearmanJob $job) {
            $workload = json_decode($job->workload());
            echo "Sending email: " . print_r($workload,1);

        });
    }

    protected function _work()
    {
        while($this->worker->work());
    }

}