<?php

abstract class App_Services_Gearman_Worker_Abstract
{
	protected $worker = null;

	public function __construct()
	{
		if (!extension_loaded('gearman')) {
			throw new RuntimeException('The PECL::gearman extension is required.');
		}

		$worker = new GearmanWorker();
		$worker->addServer(Zend_Registry::get('config')->gearman->host);
		$this->worker = $worker;
	}

	public function work()
	{
		$this->_work();
	}

	abstract protected function _work();
	
}